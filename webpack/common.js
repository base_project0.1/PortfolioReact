const { join } = require('path')

const paths = {
  root: join(__dirname, '..'), //'http://base_project0.1.gitlab.io'
  dev: join(__dirname, '..', 'src'),
  publicDev: '/',
  prod: join(__dirname, '..', 'bin'),
  publicProd: '/',
  cssAwesome: join(__dirname, '..', 'node_modules', 'font-awesome', 'css'),
  fontAwesome: join(__dirname, '..', 'node_modules', 'font-awesome', 'fonts'),
  reactSelect: join(__dirname, '..', 'node_modules', 'react-select', 'dist'),
  flexBoxGrid: join(__dirname, '..', 'node_modules', 'flexboxgrid', 'dist'),
}

module.exports = {

  paths,

  entry: ['babel-polyfill', join(paths.dev, 'index')],

  output: {
    path: paths.prod,
    filename: '[name]-[chunkhash].js',
    publicPath: paths.publicDev,
  },

  htmlPluginConfig: {
    title: '..:: Giovanni ::..',
    template: join(paths.dev, 'index.html'),
  },

  jsLoader: {
    test: /\.js$/,
    exclude: /node_modules/,
    include: paths.dev,
    use: 'babel-loader',
  },

  cssLoader: {
    test: /\.css$/,
    include: [
      paths.dev,
      paths.cssAwesome,
      paths.flexBoxGrid,
      paths.reactSelect,
    ],
    use: [
      'style-loader',
      'css-loader',
    ],
  },

  stylusLoader: {
    test: /\.styl$/,
    include: paths.dev,
    use: [
      'style-loader',
      'css-loader?modules',
      'stylus-loader'],
  },

  fileLoader: {
    test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|txt|pdf)(\?.*)?$/,
    include: [
      paths.dev,
      paths.fontAwesome,
    ],
    use: {
      loader: 'file-loader',
      query: {
        name: 'media/[name].[hash:8].[ext]',
      },
    },
  },

  urlLoader: {
    test: /\.(mp4|webm|wav|mp3|m4a|aac|oga)(\?.*)?$/,
    include: paths.dev,
    use: {
      loader: 'url-loader',
      query: {
        limit: 10000,
        name: 'media/[name].[hash:8].[ext]',
      },
    },
  },

  resolve: {
    alias: {
      dev: paths.dev,

      lib: join(paths.dev, './lib'),
      models: join(paths.dev, './models'),
      reducers: join(paths.dev, './reducers'),

      fontAwesome: join(paths.dev, './css/font-awesome.min.css'),

      components: join(paths.dev, './components'),
      images: join(paths.dev, './imgs'),
      utils: join(paths.dev, './utils'),
      pages: join(paths.dev, './components/Pages'),

      API_Utils: join(paths.dev, './API/Utils'),
      API_ComponentsBase: join(paths.dev, './API/Components/Base'),
      API_ComponentsStyled: join(paths.dev, './API/Components/Styled'),
      API_Factory: join(paths.dev, './API/Factory'),
    },
  },
}
