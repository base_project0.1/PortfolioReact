const webpack = require('webpack')
const common = require('./common')

const { join } = require('path')

const HtmlPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')

// https://www.npmjs.com/package/uglifyjs-webpack-plugin/v/1.3.0
const ImageminPlugin = require('imagemin-webpack-plugin').default

module.exports = {
  entry: common.entry,

  output: Object.assign({}, common.output, {
    publicPath: common.paths.publicProd
  }),

  plugins: [
    new CleanPlugin([common.paths.prod], {
      root: common.paths.root,
    }),

    new ExtractTextPlugin({
      filename: '[name]-[hash].css',
    }),

    // new webpack.DefinePlugin({
    //   'process.env': {
    //     NODE_ENV: '"production"',
    //     __HOST__: '"https://consulta-veiculos.nimble.com.br/v1/"',
    //     TIMEOUT: 20000,
    //     HEADERS: {
    //       'Access-Control-Allow-Origin': '"*"',
    //     },
    //   },
    //   __HOST__: '"https://consulta-veiculos.nimble.com.br/v1/"',
    //   TIMEOUT: 20000,
    //   HEADERS: {
    //     'Access-Control-Allow-Origin': '"*"',
    //   },
    // }),

    new ImageminPlugin({
      maxFileSize: 50000,
      pngquant: {
        quality: '95'
      }
    }),

    new ImageminPlugin({
      minFileSize: 50000,
      maxFileSize: 350000,
      pngquant: {
        quality: '80-90'
      }
    }),

    new ImageminPlugin({
      minFileSize: 350000,
      pngquant: {
        quality: '67-80'
      }
    }),



    // new ImageminPlugin({
    //   test: [/.*projects.*/, /.*companies.*/],
    //   pngquant: {
    //     quality: '60'
    //   }
    // }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'react-build',
      minChunks: ({ resource }) => (
        /node_modules\/react(-dom)?\//.test(resource)
      ),
    }),

    new HtmlPlugin(common.htmlPluginConfig),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
    }),
  ],

  module: {
    rules: [
      common.jsLoader,
      common.cssLoader,
      common.fileLoader,
      common.urlLoader,
      Object.assign({}, common.stylusLoader, {
        use: ExtractTextPlugin.extract({
          fallback: common.stylusLoader.use[0],
          use: common.stylusLoader.use.slice(1),
        }),
      }),
    ],
  },

  resolve: common.resolve,
}
