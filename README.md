## Pré-requisitos

 - Node.js >= v7
 - npm install yarn -g
   <!-- Instalar Yarn Global -->

 - para o acesso a API será necessário ter o plugin CORS instalado no navegador
 https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

## Roda aplicação
  - Instalar dependências `make install`
  - Rodar em ambiente de desenvolvimento `make start` na url `http://localhost:3002/`

OLD -----------------------------------------------------------------------

## Rodar aplicação

- Instalar dependências  `npm i` e depois `yarn install`
- Rodar `yarn start` (ou `npm start`) desenvolvimento online em `http://localhost:3002`
- Rodar `yarn build` (ou `npm run build`) para gerar o build de produção (Arquivos gerado no diretório `dist`)

## Scripts

- `yarn start` (ou `npm start`) : Iniciar a aplicação
- `yarn build` (ou `npm run build`) : Gerar build de produção
- `yarn update-packages` : Atualizar todas as dependências para a versão atual
