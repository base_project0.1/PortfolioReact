import React, { Component } from 'react'
import YouTube from 'react-youtube';
import PropTypes from 'prop-types'

import Style from './style'

class YoutubeVideos extends Component {

    render() {

        const {url, height, width} = this.props

        const opts = {
            height: height || '390',
            width: width || '640',
            playerVars: { // https://developers.google.com/youtube/player_parameters
              autoplay: 0
            },
            origin: window.location.origin,
            enablejsapi: 1
          };
        return (
            <YouTube
                videoId={url.replace('https://youtu.be/', '')}
                opts={opts}
                onPlay={(e) => {this.props.setVideo(e.target)}}
                onEnd={this.props.stopVideo()}
            />
        )

    }

}

export default YoutubeVideos