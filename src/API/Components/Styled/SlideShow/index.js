import React, { Component } from 'react'
import PropTypes from 'prop-types'

import YoutubeVideos from './medias/YoutubeVideos/index'
import ResponsiveBg from 'API_ComponentsBase/Images/ResponsiveBg/index'

import defaultLoading from './Ellipsis-1.4s-200px.gif'

import Style from './style'

class SlideShow extends Component {

    constructor(props) {
        super(props)

        this.lastID = props.medias.length - 1
        this.currentVideo
        this.state = {
            currentMediaID: 0,
            loading: true,
        }

        this.fullScreen = false

        document.onfullscreenchange = (e) => this.toggleFullscreen(e.target)
    }

    componentDidMount() {

        const onlyImagesMedias = this.props.medias.filter(url => !url.match("https://youtu.be/"))

        let totalMedias = onlyImagesMedias.length
        let loadedMedias = 0
        for(var i = 0; i < onlyImagesMedias.length; i++) {
            let newImage = new Image()
            newImage.onload = () => {
                loadedMedias++
                if(loadedMedias * 1.25 >= totalMedias) {
                    this.setState({loading: false}, () => this.intervalId = this.slideSetInterval())
                }
            }
            newImage.src = onlyImagesMedias[i]
        }

    }

    componentWillUnmount() {
        clearInterval(this.intervalId)
        clearInterval(this.slideIntervalID)
    }

    toggleFullscreen(element) {
        if(element.classList.contains('full-screen') == this.fullScreen) 
            element.classList[this.fullScreen ? 'remove' : 'add']('full-screen')
       
        this.fullScreen = !this.fullScreen
    }

    openFullscreen(element, useElement) {
        if(element.classList.contains('btn-control')) return

        const slideShow = useElement ? element : element.parentElement.parentElement.parentElement

        slideShow.classList.add('full-screen')
        
        if (slideShow.requestFullscreen) {
          slideShow.requestFullscreen();
        } else if (slideShow.mozRequestFullScreen) { /* Firefox */
          slideShow.mozRequestFullScreen();
        } else if (slideShow.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
          slideShow.webkitRequestFullscreen();
        } else if (slideShow.msRequestFullscreen) { /* IE/Edge */
          slideShow.msRequestFullscreen();
        }
    }

    closeFullscreen(element, useElement) {

        if(element.classList.contains('btn-control')) return

        const slideShow = useElement ? element : element.parentElement

        slideShow.classList.remove('full-screen')

        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.mozCancelFullScreen) { /* Firefox */
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
          document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE/Edge */
          document.msExitFullscreen();
        }

    }

    slideSetInterval() {
        if(this.props.slideInterval) {

            clearInterval(this.slideIntervalID)

            this.slideIntervalID = 
            setInterval(() => {
                this.fowardMedia()
            }, this.props.slideInterval)

        }
    }

    createMediaByType(url) {
        if(url.match("https://youtu.be/")) 
            return <YoutubeVideos
                        stopVideo={() => this.stopVideo()}
                        setVideo={(e) => this.setCurrentVideo(e)} 
                        url={url}
                    />
        
        return <ResponsiveBg url={url}/>

    }

    mediaCount(mediasQtt) {

        const qttChars = mediasQtt.toString().length
        const currentID = (this.state.currentMediaID + 1).toString()

        return currentID.padLeft(qttChars,"0") + "/" + mediasQtt

    }

    changeMedia(method) {
        this.stopVideo()
        this.slideSetInterval()

        if(method === 'back') {
            this.backMedia()
            return
        } 

        this.fowardMedia()
    }

    fowardMedia() {
        const next = this.state.currentMediaID + 1
        this.setState({currentMediaID: next > this.lastID ? 0 : next})
    }

    backMedia() {
        const next = this.state.currentMediaID - 1
        this.setState({currentMediaID: next < 0 ? this.lastID : next})
    }

    setCurrentVideo(currentVideo) {
        this.currentVideo = currentVideo
    }

    stopVideo() {
        if(this.currentVideo) this.currentVideo.stopVideo()
    }

    render() {

        const {medias, noControl, mediasClass} = this.props

        return (
            <Style>
                <div className="slide-show">
                    <i 
                        className="fa fa-times-circle close-full-screen" 
                        onClick={(e) => this.closeFullscreen(e.target)} 
                    />
                    <div className={["medias", mediasClass].join(" ")}>
                        {this.state.loading && 
                            <div className="loading-box">
                                <img className="loading-image" src={defaultLoading} />
                            </div>        
                        }
                        {!this.state.loading && medias && medias.map((url, id) => 
                            <div key={id} 
                                onClick={(e) => this.openFullscreen(e.target)}
                                className={[
                                    "media", (this.state.currentMediaID === id ? 'selected' : null)].join(" ")
                                }>
                                {this.createMediaByType(url)}
                            </div>
                        )}
                    </div>
                    {
                        !noControl && !this.state.loading &&
                        <div className="control">
                            <div className="btn-control btn-back" onClick={() => this.changeMedia('back')}> <span className="fa fa-angle-left" /> </div>
                            <div className="btn-control btn-foward" onClick={() => this.changeMedia()}> <span className="fa fa-angle-right"/> </div>
                            <div className="number-pages"> { this.mediaCount(medias.length) } </div>
                        </div>
                    }
                </div>
            </Style>
        )

    }

}

export default SlideShow