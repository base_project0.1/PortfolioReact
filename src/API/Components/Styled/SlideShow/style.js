import styled from 'styled-components'

const height = '50vh'

const style = styled.div`

    .slide-show {

        .medias {

            height: ${height};

            .loading-box {
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .loading-image {
                opacity: 0.5;
                filter: blur(3px);
            }

            .media {
                height: 100%;
                width: auto;
                text-align: center;
                cursor: pointer;

                img, div, iframe {
                    width: 100%;
                    max-width: 100%;
                    height: inherit;
                }

                img, div {
                    transform: translateY(-50%);
                    top: 50%;
                    position: relative;
                }
            }

            .media:not(.selected) {
                display: none;
            }

        }

        .control {

            z-index: 100000;
            font-size: 20px;
            position: relative;
            display: flex;

            .btn-control {
                width: 20px;
                position: absolute;
                
                align-self: center;
                text-align: center;
                
                cursor: pointer;
                
                display: flex;
                justify-content: center;
                align-items: center;
                
                user-select: none;

                opacity: .7;
                transition: opacity .5s;

                span {
                    border: 2px solid #00000070;
                    border-radius: 50%;
                    background-color: #ababab;
                    opacity: .8;
                    transition: opacity .5s;

                    padding: 10px 22px;
                    font-size: 36px;
                    font-weight: bold;
                }
            }

            .btn-control:hover span {
                opacity: 1;
                background-color: #696969;
            }

            .btn-back {
                border-radius: 0 5px 5px 0;
            }

            .btn-foward {
                right: 0px;
                border-radius: 5px 0 0 5px;
            }

            .number-pages {
                width: 100%;
                background-color: #d8d8d840;
                padding-top: 3px;
                text-align: center;
                border-radius: 5px 5px 0 0;
                font-weight: bold;
            }

        }

        .close-full-screen {
            display: none;
        }

        &.full-screen {
            .medias {
                height: calc(100vh - 28px) !important;
            }

            .media {
                cursor: default;
            }

            .media > div {
                width: 100%;
            }

            .control .btn-control {
                width: 36px;
                height: calc(100vh - 36px);
                top: auto;
            }

            .close-full-screen {
                width: 100%;
                position: fixed;
                font-size: 2.5em;
                display: flex;
                justify-content: center;
                z-index: 100;
            }
        }

    }

`
export default style
