import React, {Component} from 'react'
import Style from './style'

export default class ProgressBar extends Component {

  render() {

    const {text, value, classes} = this.props

    return (
      <Style className={'progress-bar ' + classes}>
        <div className="bar-inside-value" style={{width: (value * 10) + "%"}}>
          <div>{value * 10}%</div>
        </div>

        <div className="bar-inside-name">
          <div>{text}</div>
        </div>
      </Style>
    )
  }

}