import styled from 'styled-components'

const style = styled.div`

  display: flex;
  position: relative;

  .bar_inside-name,
  .bar-inside-value
  {
    display: inline-flex;
  }

  .bar-inside-value {
    position: absolute;
  }

`

export default style