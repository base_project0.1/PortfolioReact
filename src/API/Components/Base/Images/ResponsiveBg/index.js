import {Component} from 'react'

export default class ResponsiveBg extends Component {

    render() {

        const {style, url} = this.props

        return (
            <div
                className="responsive-background"
                style={{
                    width: '100%',
                    height: '100%',
                    backgroundImage: `url('${url}')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'contain',
                    backgroundPosition: 'center',
                    ...style
                }} />
        )

    }

}