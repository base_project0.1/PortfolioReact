export default class urlParams {

  constructor(url) {
    this.url = url
    this.params = this.get()
  }

  get () {
    let params = {};
    let parser = document.createElement('a');
    parser.href = this.url;
    const query = parser.search.substring(1);
    const vars = query.split('&');
    
    if(vars && vars[0].length > 0) {
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
      }
    }
    return params;
  };

  set(params, {title, state} = {title: null, state: null}, replace = false ) {
    let lastParams = replace ? {} : {...this.params}
    
    this.params = {
      ...lastParams,
      ...params
    }

    const {origin, pathname} = window.location
    this.url = origin + pathname + this.createParamsUrl()

    history.pushState(title, state, this.url)
  }

  createParamsUrl() {

    if(this.params.length == 0) return '';

    const paramsKeys = Object.keys(this.params)

    return '?'.concat(paramsKeys.map(k => `${k}=${this.params[k]}`).join('&'))

  }

}