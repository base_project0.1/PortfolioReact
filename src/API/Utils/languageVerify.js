const stringToHTML = require('html-react-parser');

const LanguageVerify = {

  getValidLanguageTxt: (configTxt, language, errorTxt) => {
    return stringToHTML(
      LanguageVerify.getValidLanguage(configTxt, language, 'default', errorTxt, '')
    )
  },

  getValidArrayImgs: (configImgs, language, errorTxt) => {
    return LanguageVerify.getValidLanguage(configImgs, language, 'defaultImgs', errorTxt, [])
  },

  getValidArrayVideos: (configVideos, language, errorTxt) => {
    return LanguageVerify.getValidLanguage(configVideos, language, 'defaultVideos', errorTxt, [])
  },

  getValidLanguage: (config, language, default_name, errorTxt, default_return) => {
    if(!config){
      console.error("getValidLanguage -> config ... undefined ");
      return default_return
    }

    const currentLgg = config[language]

    if(currentLgg){
      if(currentLgg.length > 0){return currentLgg}
    }

    const defaultConfig = config[default_name]

    return defaultConfig || errorTxt || default_return
  }

}

module.exports = LanguageVerify;
