const initialState = {
  //TODO: ptBR is a default language now, because I need to improve the translation
  current:'ptBR',
  // current:'enEN',
  languages: {
    enEN:0,
    ptBR:1,
    esES:2,
    jaJA:3,
    frFR:4,
  },
}

const types = {
  SET_LANGUAGE: 'SET_LANGUAGE',
  GET_LANGUAGE: 'GET_LANGUAGE',
}
export const actions = {
  setLanguage: (newLanguage) => (dispatch) => {
    const language = newLanguage || navigator.language.replace('-','')
    dispatch({type: types.SET_LANGUAGE, payload: language})
  },
}
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_LANGUAGE:
    return {
      ...state,
      current: payload,
    }
    default:
      return state
  }
}
