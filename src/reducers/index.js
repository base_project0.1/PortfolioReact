import { combineReducers } from 'redux'

import languagesReducer from './Language'

export default combineReducers({
  languagesReducer,
})
