import React, { Component } from 'react'

import Style from './style'

import { BrowserRouter, Route, Router, Switch, Redirect } from "react-router-dom"
import { createBrowserHistory } from 'history';

import Header from 'pages/Fixed/header'

import Body from 'pages/Home/'
import Projects from 'pages/Projects/'
import Curriculum from 'pages/Curriculum/'
import CV from 'pages/Curriculum/components/cv'
import Contact from 'pages/Contact/'
import About from 'pages/About'

import Footer from 'pages/Fixed/footer'

import routes from './routes/routes'

const history = createBrowserHistory();

class HomePage extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
      <Style>
        <Router history={history}>
          <div className="body"> 
            <Switch>
              <Route path={routes.HomePage} component={Body} />
              <Route path={routes.Projects} component={Projects} />
              <Route path={routes.Curriculum} component={Curriculum} />
              <Route path={routes.CV} component={CV} />
              <Route path={routes.Contact}  component={Contact} />
              <Route path={routes.About} component={About} />
              <Route render={() => <Redirect to={routes.HomePage} />} />
            </Switch>
            <Header />
            <Footer />
          </div>
        </Router>
      </Style>
    )
  }
}

export default HomePage
