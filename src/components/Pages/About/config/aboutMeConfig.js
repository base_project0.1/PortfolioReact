// import me_palmeiras from 'images/about/me_palmeiras.jpg'
// import me_capitolio from 'images/about/me_capitolio.jpg'
import me_holambra from 'images/about/me_holambra.jpg'
// import me_moto from 'images/about/me_moto.jpg'
// import me_guitar from 'images/about/guitar.png'
// import me_curitiba from 'images/about/curitiba.jpg'
import aus_kan from 'images/about/aus_kan.png'
import aus_opera from 'images/about/aus_opera.jpg'
import me_guitar2 from 'images/about/guitar2.png'

export default {
  
  "photosUrl": [
    // me_moto,
    // me_curitiba,
    // me_guitar,
    me_guitar2,
    // me_palmeiras,
    // me_capitolio,
    aus_opera,
    me_holambra,
    aus_kan,
  ],

  "paragraphs":[
  {
    "ptBR": "Olá! Sou Giovanni Pregnolato Rosim, formado em Jogos Digitais na FATEC de Americana e sou apaixonado pela área de desenvolvimento de jogos e web front-end.",
    "enEN": "Hello! I'm Giovanni Pregnolato Rosim, I'm gradueted in Game Developer at FATEC Americana (Brazil, São Paulo State) and I'm fall in love for game and front-end development area."
  },
  {
    "ptBR": "Estou sempre em busca de oportunidades onde possa me desenvolver técnicamente e como pessoa, em um ambiente jovem e propício para trocas de experiências e conhecimentos, onde todos possam se ajudar durante a jornada de trabalho.",
    "enEN": "I'm always seeking for opportunities that improve me technically and as a person, in an young atmosphere and friendly, to share experiences and knowlegde, where all colleagues can help one with others during the job."
  },
  {
    "ptBR": "Tenho um espirito jovem, otimista e mente aberta; gosto de estar sempre aprendendo, sou bastante curioso. Resolvo as situações com calma e organização, além de ser proativo, ajudando sempre meus companheiros.",
    "enEN": "I have a young soul, optimistic and open mind; I'm a curios person and I love to be always learning. I'm solve the situations with calmness and organization, besides I'm be proactive and helping all my people."
  }],

  "hobbies": [
    {"ptBR": "Jogos", "enEN": "Games"},
    {"ptBR": "Viajar", "enEN": "Travel"},
    {"ptBR": "Guitarra", "enEN": "Eletric guitar"},
    {"ptBR": "Futebol", "enEN": "Soccer"},
    {"ptBR": "Cozinhar", "enEN": "Cook"}
  ]
}
