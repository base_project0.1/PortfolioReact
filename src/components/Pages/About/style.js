import styled from 'styled-components'

const style = styled.div`

  height: 100%;
  position: absolute;
  width: 100%;

  padding-top: 25px !important;

  background-color: black;

  .slideShow {
    height: 50vh;
    min-height: 300px;
    width: 100%;
    background: linear-gradient(to right,#21212150 ,#67676750,#21212150);

    margin: 30px 0px;

    border: 1px solid #ffffff45;
    border-radius: 5px;
    padding: 10px 0px;

    li {
        background-size: contain;
        z-index: 0;
    }

    .show-index {
      opacity: 0;
    }
  }

  .about-me {
    background: linear-gradient(to right,#21212150 ,#67676750,#21212150);
    margin: 30px;
    margin-bottom: 60px;
    padding: 10px 30px;
    border: 1px solid #ffffff45;
    border-radius: 5px;

    color: white;
    font-size: 1.3em;

    .paragraphs {
      text-align: justify;
    }
    .paragraphs::first-letter {
      margin-left: 30px;
    }

    .hobbies-title {
      border-bottom: 1px solid #ffffff5c;
      margin-bottom: 5px;
    }

    .hobbies {
      display: inline-flex;
      padding: 5px;
      margin: 5px;
      background: #585858;
      border: white;
      border-radius: 5px;
      pointer-events: none;
    }
  }

`
export default style
