import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from "react-router-dom";

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import MySlideShow from 'API_ComponentsStyled/SlideShow/index'

import Style from './style'

import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

import aboutMeConfig from './config/aboutMeConfig.js'

import TweenMax from 'gsap'

class About extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {

    TweenMax.fromTo('.slideShow', .5, 
      {opacity: 0, top: '-=200', position: 'relative'},
      {opacity: 1, top: '+=200', position: 'relative'},
    )

    TweenMax.to('.about-me > div,p', 0, {opacity: 0})

    TweenMax.fromTo('.about-me', .5, 
      {opacity: 0, top: '+=200', position: 'relative'},
      {opacity: 1, top: '-=200', position: 'relative',
      onComplete: () => {
        TweenMax.staggerTo('.about-me > div,p', .1, {opacity: 1}, .25)
      }}
    )

  }

  render() {

    const {photosUrl, paragraphs, hobbies} = aboutMeConfig
    const {current} = this.props

    return (
      <Style>
        <div style={{color:'white'}}>
          <div className="slideShow">
            <MySlideShow 
              medias={photosUrl}
              slideInterval={3500}
              noControl={true}
            />
          </div>
        </div>
        <div className="about-me">
          {
            paragraphs && paragraphs.length > 0 && paragraphs.map((item, i) => {
              return <p key={i} className="paragraphs">{getValidLanguageTxt(item, current)}</p>
            })
          }
          {
            hobbies && hobbies.length > 0 && [<div key="hobbies-title" className="hobbies-title">Hobbies</div>].concat(
              hobbies.map((item, i) => {
              return <div key={i} className="hobbies">{getValidLanguageTxt(item, current)}</div>
            }))
          }
        </div>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(About)
