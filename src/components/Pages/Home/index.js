import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

// import Pagination from 'components/Pagination'
//
import Loading from 'components/Loading'

// import 'fontAwesome'

import { Link } from "react-router-dom";

import photo from
 // 'images/menu/eu3d1.jpg'
//  'images/menu/3x4.jpg'
//  'images/menu/eu3d1_2.jpg'
// 'images/menu/eu.png'
// 'images/menu/eu1.png'
// 'images/menu/eu2.jpg'
// 'images/menu/eu3.jpg'
'images/menu/eu_face.png'

import imgProjects from
'images/menu/code2.jpeg' 
// 'images/menu/code.png'
// 'images/menu/proj1.jpg'

import imgCurriculum from 'images/menu/cv3.jpg'
import imgContact from 
'images/menu/send_mail.jpg'
// 'images/menu/cont1.jpg'

import imgAbout from 'images/menu/ab1.jpg'

import config from './config/config.json'

import TweenMax from 'gsap'

import routes from '../../../routes/routes'

import LggeFct, {getValidLanguageTxt, getValidArrayImgs} from 'API_Utils/languageVerify.js'

class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      loading: document.readyState != "complete"
    }
    
  }

  componentDidMount() {
    window.onload = () => this.setState({loading: false}, this.onLoading())
    !this.state.loading && this.onLoading()
  }

  onLoading() {

    document.querySelector('.body > div').style.overflowY = 'hidden'

    window.scrollTo(0, 0);

    const {
      projects, curriculum, contact, about,
      underConstruction
    } = config

    let divProject = document.querySelector([".squad.", projects.default].join(""))
    let divCurriculum = document.querySelector([".squad.", curriculum.default].join(""))
    let divContact = document.querySelector([".squad.", contact.default].join(""))
    let divAbout = document.querySelector([".squad.", about.default].join(""))

    let divCentralPhoto = document.querySelector(".central-circle")

    TweenMax.from(divProject, 1, {left: '-500', top: '-500'})
    TweenMax.from(divCurriculum, 1, {right: '-500', top: '-500'})
    TweenMax.from(divContact, 1, {left: '-500', bottom: '-500'})
    TweenMax.from(divAbout, 1, {right: '-500', bottom: '-500'})

    TweenMax.from(divCentralPhoto, 1, {
      opacity: 0, delay: 1, scale: 2, 
      onComplete: ()=> {
        divCentralPhoto.style.transform = 'translate(-50%, -50%)'
        window.scrollTo(0, 0);
      }
    })

    let allMenuLabels = document.querySelectorAll(".menu-labels")
    TweenMax.from(allMenuLabels, 0.5, {
      opacity: 0, delay: 1.25, scale: 2, ease: Elastic.easeOut,
      onComplete: () => allMenuLabels.forEach(lbl => lbl.style.transform = '') 
    })

  }

  SelectedMenuItem(itemName, to) {
    const routeName = itemName.charAt(0).toUpperCase() + itemName.substr(1)
    let divProject = document.querySelector([".squad.", itemName].join(""))

    TweenMax.to(divProject.querySelector(".menu-labels"), 0, {opacity: 0})

    TweenMax.to(divProject, 1, {position: "absolute", width: '100%', height: '100%', zIndex: 3, ...to})
    TweenMax.to(divProject.querySelector('img'), 1, {
      opacity: 1,
    })
    TweenMax.to(divProject.querySelector('.menu-labels'), 1, {
      opacity: 0, scale: 3, 
      onComplete: () => {
        this.props.history.push(routes[routeName]);
      }
    })
  }

  render() {

    const {current} = this.props
    const {
      projects, curriculum, contact, about,
      underConstruction
    } = config

    return (
      <Style>
        <div className="home">
          <Loading isLoading={this.state.loading} />
          <div className={["squad", projects.default].join(" ")} style={{left: 0, top: 0}}
            onClick={() => this.SelectedMenuItem(projects.default)}>
              <div style={{backgroundColor:'rgba(255,0,0,0.25)'}}></div>
              <img src={imgProjects}/>
              <i className="fa fa-gamepad" />
              <div className="menu-labels" style={{top:'50%'}}>
                <label>{getValidLanguageTxt(projects, current)}</label>
              </div>
          </div>

          <div className={["squad", curriculum.default].join(" ")} style={{right: 0, top: 0}}/*title={getValidLanguageTxt(underConstruction, current)}*/
            onClick={() => this.SelectedMenuItem(curriculum.default)}>
              <div style={{backgroundColor:'rgba(0,0,255,0.25)'}}></div>
              <img src={imgCurriculum}/>
              <i className="fa fa-graduation-cap" />
              <div className="menu-labels" style={{top:'50%'}}>
                <label>{getValidLanguageTxt(curriculum, current)}</label>
              </div>
          </div>

          <div className={["squad", contact.default].join(" ")} style={{left: 0, bottom: 0}}/*title={getValidLanguageTxt(underConstruction, current)}*/
            onClick={() => this.SelectedMenuItem(contact.default)}>
              <div style={{backgroundColor:'rgba(0,255,255,0.25)'}}></div>
              <img src={imgContact}/>
              <i className="fa fa-envelope" />
              <div className="menu-labels" style={{top:'50%'}}>
                <label>{getValidLanguageTxt(contact, current)}</label>
              </div>
          </div>

          <div className={["squad", about.default].join(" ")} style={{right: 0, bottom: 0}}/*title={getValidLanguageTxt(underConstruction, current)}*/
            onClick={() => this.SelectedMenuItem(about.default)}>
              <div style={{backgroundColor:'rgba(0,255,0,0.25)'}}></div>
              <img src={imgAbout}/>
              <i className="fa fa-comment" />
              <div className="menu-labels" style={{top:'50%'}}>
                <label>{getValidLanguageTxt(about, current)}</label>
              </div>
          </div>

          <div className="central-circle">
            <img src={photo}/>
          </div>

        </div>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
