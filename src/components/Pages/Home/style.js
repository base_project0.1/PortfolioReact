import styled from 'styled-components'

const style = styled.div`

  height: 100%;
  position: absolute;
  width: 100%;

  .squad-disabled {
    opacity: 0.3;
    user-select: none;
    pointer-events: unset;
    cursor: auto !important;
  }

  .squad.projects i {
    top: -8px;
    font-size: 6.5em;
  }

  .squad.about i {
    top: -3px;
  }

  .squad.curriculum i {
    font-size: 5.6em;
  }

  .squad {
      cursor: pointer;
      position: absolute;
      height: 50%;
      width: 50%;
      display: inline-block;
      overflow: hidden;
      img {
        min-width: 100%;
        min-height: 100%;
        position: relative;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        transition: transform 1.5s;
        filter: blur(3px);
        opacity: 0.5;
      }
      i {
        height: 100%;
        font-size: 6em;
        color: white;
        display: flex;
        justify-content: center;
        align-items: center;

        opacity: 0.15;
        text-shadow: 3px 3px black;

        position: relative;
        z-index: 0;

        transition: transform 1.5s;
      }
      @media only screen and (max-width: 768px) {
        i {
          font-size: 4em !important;
        }
    }
      div {
        width: 100%;
        height: 100%;
        position: absolute;
        z-index: 1;
      }

      .menu-labels {
        font-size: 1.2rem;
        position: absolute;
        color: white;
        text-shadow: 0px 0px 5px black;
        border-radius: 5px;
        // transform: translateY(-50%);
        transform: translate(calc(25vw - 50%), -50%);
        background: radial-gradient(rgba(187, 187, 187, 0.5), rgba(146, 146, 146, 0.5), rgba(39, 39, 39, 0.5));
        width: auto;
        height: auto;
        padding: 5px;
        transition: transform .5s;
        // box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.33);

        label {
          cursor: pointer;
        }

      }

      @media only screen and
        (min-width: 700px),
        (min-height: 600px)
      {font-size: xx-large;}
      @media only screen	and
        (max-width: 699px),
        (max-height: 599px)
      {font-size: x-large;}
      @media only screen	and
        (max-width: 599px),
        (max-height: 399px)
      {font-size: large;}

    }

    .squad:hover {
      box-shadow: black 0px 0px 20px 10px;
      z-index: 2;
      filter: brightness(1.15);
    }

    .squad:hover img {
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%) scale(1.25);
    }
    .squad:hover i {
      transform: scale(1.25);
      opacity: 0.35;
    }

    .squad:hover div.menu-labels{
      transform: translate(calc(25vw - 50%), -50%) scale(1.5);
    }

    .menu-labels::first-letter{
      text-transform: uppercase;
    }

    .central-circle {
      z-index: 2;
      width: 30%;
      height: 50%;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);

      transition: transform 1.5s;

      img {
        pointer-events: none;
        height: 80%;
        @media only screen and
        (min-width: 700px) {
          height: 100%;
        }
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        position: relative;
        border-radius: 50%;
        box-shadow: 0px 0px 20px 10px #2d2d2dd1;

        transition: transform 1.5s;
      }
    }
    .central-circle:hover img {
      filter: brightness(1.05);
      transform: translate(-50%, -50%) scale(1.05);
    }

`
export default style
