import styled from 'styled-components'

const style = styled.div`

  height: 100%;
  width: 100%;

  padding-top: 50px !important;

  .companies {
    .companie-card {
      padding: 10px;
      margin: 5px;
      border: 1px solid #0000001c;
      border-radius: 5px;
      background-color: #0000001a;
      text-align: justify;
    }

    @media only screen and (max-width: 768px) {
      .companie-card {
        text-align: center;
      }
    }

    .companie-job {
      border-bottom: 1px #00000070 solid;
      margin-bottom: 5px;
      padding-bottom: 5px;
    }

    .companie-img {
      width: 150px;
      height: 150px;
      background-color: white;
    }
    .companie-img-small {
      width: 100px;
      height: 100px;
      background-color: white;
    }

    @media only screen and (max-width: 768px) {
      .companie-img-small {
        display: inline-block;
        margin-bottom: 10px;
      }
    }


    img {
      position: relative;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      max-width: 100%;
      max-height: 100%;
    }

  }

  .curriculum-page {

    padding-bottom: 60px;

    .title {
      transition: transform 1s;
    }

    .title:hover {
      transform: scale(1.2);
    }

    .btn-download-cv {
      text-decoration: none;
      color: white;
      font-size: larger;
      font-weight: bold;
      text-align: center;
      div {
        transition: transform 1s;
        margin-bottom: 10px;
      }
    }

    .btn-download-cv:hover div {
        background-color: aliceblue;
        color: slategrey;
        transform: scale(1.2);
    }

    .collapse-group{

      margin: 15px 10px;
      border-radius: 10px;
      overflow: hidden;
      background-color: #646464;

      .header-tab {
          cursor: pointer;
          background: #515151;
          text-align: center;
          padding: 5px;
          border: 1px solid #00000047;
          min-height: 36px;
          line-height: 36px;
      }

      a, a:visited {
        text-decoration: none;
        background-color: #eaeaeaf0;
        border-radius: 5px;
        padding: 1px 5px;
        color: #646464;
        font-size: .9em;
        transition: .25s;
        display: inline-block;
      }

      a:hover {
        font-size: 1em;
        color: white;
        background-color: #9a9a9a;
        border: 1px solid #0000002e;
      }

      .company-projects {
        display: flex;
        width: 100%;
        padding: 5px;
        justify-content: center;
        margin-top: 15px;
        flex-wrap: wrap;

        @media (min-width: 768px) {
          justify-content: left;
        }

        a {
          margin: 0 3px;
          display: inline-flex;
          align-items: center;
          margin-bottom: 10px;
        }
      }

      .content-tab {
        padding: 10px;

        .desc-list {

          .desc {
            margin-left: 25px;
          }

          li {
            padding: 5px 0 5px 0;
          }

        }

      }

    }

  }

`
export default style
