import logo from 'images/companies/tc.jpg'

export default {
  "name": {
    "default":"TradersClub",
    "ptBR": "TradersClub",
    "enEN": "TradersClub"
  },
  "period": {
    "init": "02/2018",
    "end": "06/2018"
  },
  "job": {
    "ptBR": "Programador Front-End Web e Mobile",
    "enEN": "Web and Mobile Front-End Developer"
  },
  "activities": {
    "ptBR": "Desenvolvimento do chat do site e aplicativo sobre informações sobre mercado e bolsa de valores, usando React, React-Native e Node, com o sistema operacional Linux.",
    "enEN": "development a chat of the site and app about news of financial market and stock exchange, using React, React-Native and Node, with Linux operacional system."
  },
  "link": {
    "desc":{
      "ptBR": "",
      "enEN": ""
    },
    "url": {
      "default":"",
      "ptBR": "",
      "enEN": ""
    }
  },
  "photoUrl": logo
}
