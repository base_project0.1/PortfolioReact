import logo from 'images/companies/just.png'

export default {
  "name": {
    "default":"Just Digital",
    "ptBR": "Just Digital",
    "enEN": "Just Digital"
  },
  "period": {
    "init": "10/2018",
    "end": "03/2020"
  },
  "job": {
    "ptBR": "Programador Front-End Web",
    "enEN": "Web Front-End Developer"
  },
  "activities": {
    "ptBR": "Desenvolvimento de sites com <u>Drupal CMS</u>, junto com <u>Vue.js, Gulp, Docker</u>, além de alguns projetos com <u>React</u>. <i>Metodologia Ágil</i> e <i>SCRUM</i>, participando de diversas reuniões junto ao cliente.",
    "enEN": "Development of sites with <u>Drupal CMS</u>, together <u>Vue.js, Gulp, Docker</u>, beside some projects using <u>React</u>, all using Linux operacional system. The company uses <i>Agile Methodology</i> and <i>SCRUM</i> for manager your projects.I participate often in meeti</u>ngs with the custume</u>r."
  },
  "links": [
    {
      "desc":{
        "default":"Toyota DDS",
        "ptBR": "",
        "enEN": ""
      },
      "url": {
        "default":"/projects/toyota_dds",
        "ptBR": "",
        "enEN": ""
      }
    },
    {
      "desc":{
        "default":"OdontoPrev",
        "ptBR": "",
        "enEN": ""
      },
      "url": {
        "default":"/projects/odontoprev_benef",
        "ptBR": "",
        "enEN": ""
      }
    },
    {
      "desc":{
        "default":"Sulamérica",
        "ptBR": "",
        "enEN": ""
      },
      "url": {
        "default":"/projects/sulamerica_prest",
        "ptBR": "",
        "enEN": ""
      }
    },
    {
      "desc":{
        "default":"Christmas 2020",
        "ptBR": "Natal 2020",
        "enEN": "Christmas 2020"
      },
      "url": {
        "default":"/projects/natal_just_2020",
        "ptBR": "",
        "enEN": ""
      }
    }
  ],
  "photoUrl": logo
}
