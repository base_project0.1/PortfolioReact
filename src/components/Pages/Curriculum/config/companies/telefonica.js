import logo from 'images/companies/telefonica.png'

export default {
  "name": {
    "default":"Telefônica",
    "ptBR": "Telefônica - Educação Digital",
    "enEN": "Telefônica - Digital Education"
  },
  "period": {
    "init": "07/2016",
    "end": "10/2017"
  },
  "job": {
    "ptBR": "Programador Javascript.",
    "enEN": "Javascript Developer"
  },
  "activities": {
    "ptBR": "Desenvolvimento de e-learnings multiplataformas para aplicativos e jogos, para auxiliar na educação de crianças africanas e para treinamentos de euipes de outras empresas. Desenvolvido em <u>Javascript</u>, junto com a API do <u>PIXI.js</u>, o <u>Node</u> e <u>React</u>.",
    "enEN": "Development of multplataforms e-learnings for apps and games, to help in education of african children and for team training of other companies. Develop in <u>Javascript</u>, together with <u>PIXI.js</u> API, <u>Node</u> and <u>React</u>."
  },
  "links": [
    {
      "desc":{
        "default":"E-learnings",
        "ptBR": "",
        "enEN": ""
      },
      "url": {
        "default":"/projects/ted",
        "ptBR": "",
        "enEN": ""
      }
    },
    {
      "desc":{
        "default":"Pró Futuro",
        "ptBR": "Pró Futuro",
        "enEN": "Pro Future"
      },
      "url": {
        "default":"/projects/profuturo",
        "ptBR": "",
        "enEN": ""
      }
    },
  ],
  "photoUrl": logo
}
