import logo from 'images/companies/s2d.gif'

export default {
  "name": {
    "default":"S2D Supply 2 Demand Integration",
    "ptBR": "S2D Supply 2 Demand Integration",
    "enEN": "S2D Supply 2 Demand Integration"
  },
  "period": {
    "init": "01/2012",
    "end": "10/2013"
  },
  "job": {
    "ptBR": "Desenvolvedor e Suporte (Estágio)",
    "enEN": "Developer and System Suport (Trainee)"
  },
  "activities": {
    "ptBR": "Desenvolvimento de sistema, programando em linguagem própria da empresa, usando o Oracle para gerenciar o banco de dados e o Crystal Report para os relatórios, assim como auxiliar o usuário a usar da melhor maneira o sistema.",
    "enEN": "System development, using own company programmation language, Oracle as the data base, and Crystal Report, besides help the user with the system."
  },
  "photoUrl": logo
}
