import logo from 'images/companies/gv2c.png'

export default {
  "name": {
    "default":"GV2C Gather Value to Costumer",
    "ptBR": "GV2C Gather Value to Costumer",
    "enEN": "GV2C Gather Value to Costumer"
  },
  "period": {
    "init": "10/2017",
    "end": "02/2018"
  },
  "job": {
    "ptBR": "Programador Front-End Web",
    "enEN": "Web Front-End Developer"
  },
  "activities": {
    "ptBR": "Desenvolvimento do sistema de Comércio Exterior da empresa AMBEV, usando <u>React</u> e <u>Node</u>, além de discutir soluções de layout e funcionamento do mesmo.",
    "enEN": "Development of the system that manage the foreign trade of AMBEV company, using <u>React</u> and <u>Node</u>, besides meetings to discuss the solutions of layout and operation of system."
  },
  "links": [
    {
      "desc":{
        "default":"Ambev",
        "ptBR": "",
        "enEN": ""
      },
      "url": {
        "default":"/projects/ambev_comex",
        "ptBR": "",
        "enEN": ""
      }
    },
  ],
  "photoUrl": logo
}
