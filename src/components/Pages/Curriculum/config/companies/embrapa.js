import logo from 'images/companies/embrapa.png'

export default {
  "name": {
    "default":"EMBRAPA",
    "ptBR": "EMBRAPA Emp. Brasileira de Pesquisas Agropecuárias",
    "enEN": " EMBRAPA Brazilian Agricultural Research Company"
  },
  "period": {
    "init": "03/2014",
    "end": "07/2014"
  },
  "job": {
    "ptBR": "Programador C++ (Estágio)",
    "enEN": "C++ Developer (Trainee)"
  },
  "activities": {
    "ptBR": "Implementação do Framework, em <u>C++</u>, utilizado para simulação dinâmica através de modelos de crescimento do gado, para treinamento de pecuaristas.",
    "enEN": "Framework development, using <u>C++</u>, used to dynamic simulation, through of cattle growth model, for improve the profits of cattle ranchers."
  },
  "photoUrl": logo
}
