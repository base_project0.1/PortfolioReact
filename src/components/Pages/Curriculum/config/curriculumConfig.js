import justdigital from "./companies/justdigital.js"
import tradersClub from "./companies/tradersclub.js"
import gv2c from "./companies/gv2c.js"
import telefonica from "./companies/telefonica.js"
import embrapa from "./companies/embrapa.js"
import s2d from "./companies/s2d.js"

export default {

  "goals": [{
    "ptBR": "Trabalhar como desenvolvedor de jogos, web front-end (com preferência para React.js) ou sistemas, em uma empresa com um ambiente jovem, com possibilidade de aprendizado e troca de conhecimento.",
    "enEN": "Work with game, web or system developer in a company with ambience that have a possibility of learn and share knowledge.",
  }],

  "education": [{
    "ptBR": "Graduação em Jogos Digitais. FATEC Americana, conclusão em 2014.",
    "enEN": "Graduation in Digital Games in FATEC Americana, conclusion at 2014",
  }],

  "profissional_experience": [
    justdigital,
    // tradersClub,
    gv2c,
    telefonica,
    embrapa,
    s2d,
  ],

  "myLanguages": [
    {
      "ptBR": "Português: Nativo",
      "enEN": "Portuguese: Native",
    },
    {
      "ptBR": "Inglês: Intermediário (B1)",
      "enEN": "English: Intermediate (B1)",
    }
  ],

  "myCourses": [
    {
      "name": {
        "default": "Next Level Week 3.0 - Rocketseat",
      },
      "desc": {
        "ptBR": "Uma semana de curso desenvolvendo o aplicativo <a href='/projects/happy' target='blank'>Happy</a> utilizando <u>React, React Native e Node.js</u>.",
        "enEN": "Development of the app <a href='/projects/happy' target='blank'>Happy</a> using <u>React, React Native and Node.js</u>.",
      }
    },
    {
      "name": {
        "default": "Next Level Week 2.0 - Rocketseat",
      },
      "desc": {
        "ptBR": "Uma semana de curso desenvolvendo o aplicativo <a href='/projects/proffy' target='blank'>Proffy</a> utilizando <u>React, React Native e Node.js</u>.",
        "enEN": "Development of the app <a href='/projects/proffy' target='blank'>Proffy</a> using <u>React, React Native and Node.js</u>.",
      }
    },
    {
      "name": {
        "default": "Next Level Week 1.0 - Rocketseat",
      },
      "desc": {
        "ptBR": "Uma semana de curso desenvolvendo o aplicativo <a href='/projects/ecoleta' target='blank'>Ecoleta</a> utilizando <u>React, React Native e Node.js</u>.",
        "enEN": "Development of the app <a href='/projects/ecoleta' target='blank'>Ecoleta</a> using <u>React, React Native and Node.js</u>.",
      }
    },
    {
      "name": {
        "default": "OmniStack Week 11.0 - Rocketseat",
        "ptBR": "Semana OmniStack 11.0 - Rocketseat",
        "enEN": "OmniStack Week 11.0 - Rocketseat",
      },
      "desc": {
        "ptBR": "Uma semana de curso desenvolvendo o aplicativo <a href='/projects/be-the-hero' target='blank'>Be The Hero</a> utilizando <u>React, React Native e Node.js</u>.",
        "enEN": "Development of the app <a href='/projects/be-the-hero' target='blank'>Be The Hero</a> using <u>React, React Native and Node.js</u>.",
      }
    },
    {
      "name": {
        "default": "WordPress Course: making a website from zero.",
        "ptBR":"Curso em Video - Curso de WordPress: criando um site do zero.",
        "enEN": "WordPress Course: making a website from zero."
      },
      "desc": {
        "ptBR": "Curso que ensina a criar um site em profissional no WordPress, aprendendo boas práticas de utilização da ferramenta e tendo como resultado o <a href='/projects/fresco-wp' target='blank'>site Fresco</a>.",
        "enEN": "Course teach to make a professional website using WordPress plataform. The final result was the <a href='/projects/fresco-wp' target='blank'>website Fresco</a>.",
      }
    },
    {
      "name": {
        "default": "English Live"
      },
      "desc": {
        "ptBR": "Curso de inglês com professores particulares e aulas em grupo.",
        "enEN": "English class with private teachers and group classes.",
      }
    },
    {
      "name": {
        "default": "(Udemy) The Ultimate Guide to 2D Mobile Game Development with Unity:"
      },
      "desc": {
        "ptBR": "Desenvolvimento do <a href='/projects/dungeon_escape' target='blank'>Dungeon Escape</a> para Android.",
        "enEN": "Development of <a href='/projects/dungeon_escape' target='blank'>Dungeon Escape</a> for Android devices.",
      }
    },
    {
      "name": {
        "default": "(Udemy) The Ultimate Guide to Game Development with Unity:"
      },
      "desc": {
        "ptBR": "Desenvolvimento de jogos 2D e 3D.",
        "enEN": "Development of 2D and 3D games.",
      }
    },
    {
      "name": {
        "default": "MCSD (Microsoft Certified Solutions Developer):"
      },
      "desc": {
        "ptBR": "20480: HTML5, CSS3 e JavaScript (Visual Studio)",
        "enEN": "20480: HTML5, CSS3 and JavaScript (Visual Studio)",
      }
    },

  ],

  "mySkills": [
    {
      "ptBR": "Front-End: React, Javascript, HTML5, CSS3, Jquery, React-Native.",
      "enEN": "Front-End: React, Javascript, HTML5, CSS3, Jquery, React-Native.",
    },
    {
      "ptBR": "Back-End: Node.js.",
      "enEN": "Back-End: Node.js.",
    },
    {
      "ptBR": "Outras Linguagens: PHP, C# (Unity 3D).",
      "enEN": "Others Languages: PHP, C# (Unity 3D).",
    },
    {
      "ptBR": "Banco de Dados: mySQL, Oracle.",
      "enEN": "Data Base: mySQL, Oracle.",
    },
    {
      "default": "Scrum",
    }
  ],

  "cvUrl": {
    "default": "https://drive.google.com/file/d/1WW1CiCjdhVvVUKSrLZZfyrWMIYxDMO1V/view?usp=sharing",
    "ptBR": "https://drive.google.com/file/d/1334lCCOMIUhomNJRiQSx-BIXFCfGYhL3/view?usp=sharing",
    "enEN": "https://drive.google.com/file/d/1WW1CiCjdhVvVUKSrLZZfyrWMIYxDMO1V/view?usp=sharing",
  }
}
