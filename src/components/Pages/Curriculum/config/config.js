//Configs
import curriculumConfig from './curriculumConfig.js'
import translateConfig from './translateConfig.json'

//Utils
import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

//Components
import Companies from "../components/companies.js"
import List from "../components/simple_list.js"
import DescList from "../components/desc_list.js"
import SkillGroup from "../components/skills_group"

export default (current) => {

  const {goals, education, profissional_experience, myLanguages, myCourses, mySkills, cvUrl} = curriculumConfig
  const {objetive, degree, experience, download_cv, languages, courses, skills} = translateConfig

  return {
    tabs:[
      {
        id: "goals",
        title: getValidLanguageTxt(objetive, current),
        TabComponent: List,
        params: {translated_list: goals},
      },
      {
        id: "education",
        title: getValidLanguageTxt(degree, current),
        TabComponent: List,
        params: {translated_list: education},
      },
      {
        id: "profissional_experience",
        title: getValidLanguageTxt(experience, current),
        TabComponent: Companies,
        params: {profissional_experience},
      },
      {
        id: "my_languages",
        title: getValidLanguageTxt(languages, current),
        TabComponent: List,
        params: {translated_list: myLanguages},
      },
      {
        id: "my_courses",
        title: getValidLanguageTxt(courses, current),
        TabComponent: DescList,
        params: {translated_list: myCourses},
      },
      {
        id: "my_skills",
        title: getValidLanguageTxt(skills, current),
        TabComponent: SkillGroup,//List,
        params: {translated_list: mySkills},
      },
    ],

    cv_download: {
      href: getValidLanguageTxt(cvUrl, current),
      label: getValidLanguageTxt(download_cv, current),
    }
  }

}
