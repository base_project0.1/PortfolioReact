import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect, advancedConnect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
// import Style from './style'

import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

class SimpleList extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    const {current, translated_list} = this.props

    return (
        <div className="desc-list">
          {
            translated_list && translated_list.map(
              (item, id) => {
                const {name, desc} = item
                return (
                  <li key={id}>
                    {getValidLanguageTxt(name, current)}
                    <div className="desc">&rarr; {getValidLanguageTxt(desc, current)}</div>
                  </li>
                )
              }
            )
          }
        </div>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpleList)
