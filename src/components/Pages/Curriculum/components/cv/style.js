import styled from 'styled-components'

export default styled.div`

    font-size: 12pt;
    font-family: Montserrat;

    height: 100% !important;

    .main-title {
      font-size: 15pt;
      margin-bottom: 0.5cm;
      padding: 0.2cm;
      text-align: center;
    }

    .title {
      font-weight: normal;
      background-color: #e7e7e7;
    }

    .companie-tile {
      font-weight: bold;
      color: #414751;
      margin: 0.25cm 0;
    }

    .companies .companie-card:not(:first-child) {
      border-top: solid 1px #e7e7e7;
      padding-top: 0.1cm;
      margin-top: 0.15cm;
    }

    .companie-job {
      font-weight: bold;
      color: #414751;
    }

    .cv-title {
      margin-bottom: 0.25cm;
    }

    .cv-block.goals li {
      display: block;
    }

    .cv-content {
      text-align: justify;
      width: 100%;
    }

    .desc-list li:not(:first-child) {
      margin-top: 0.25cm;
    }

    .progress-bar {
      background-color: white;
      border: 1px solid #b5b5b5;
    }

    .bar-inside-value {
      background-color: #cccccc;
      padding: 5px !important;
    }

    .bar-inside-name {
      background-color: #b5b5b5;
      padding: 5px !important;
      color: 
    }

    hr {
      border-color: #e7e7e7 !important;
    }

    .companie-card:last-child {
      page-break-before: always;
    }

  .companie-card .company-img , img {
    display: none;
  }

`