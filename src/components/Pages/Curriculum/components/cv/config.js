export default {
  "name": {
    "ptBR": "Giovanni Pregnolato Rosim",
    "enEN": "Rosim, Giovanni P."
  },
  "main_desc": {
    "ptBR": "Brasileiro, solteiro, 27 anos",
    "enEN": "Brazilian, 27, single"
  },

  "address": {
    "ptBR": "Rua Alexandre Benois, 17, Apto 146 B, Vila Andrade – São Paulo - SP",
    "enEN": "Alexandre Benois Street, 17, Apto 146 B, Vila Andrade – São Paulo - SP"
  },

  "contacts": {
    "ptBR": "Telefone: (11) 99559-3220 / E-mail: giovanni_pr@hotmail.com",
    "enEN": "Phone: +55 (11) 99559-3220 / E-mail: giovanni_pr@hotmail.com"
  },

  "portfolio": {
    "ptBR": "Portfólio: https://gdev.netlify.com/",
    "enEN": "Portfólio: https://gdev.netlify.com/"
  }
}