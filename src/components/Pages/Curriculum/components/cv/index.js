import React, {Component} from 'react'

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

//API & Reducers
import { connect, advancedConnect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

//Utils
import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

//Configs
import config from '../../config/config.js'
import configCV from './config'

import Style from './style'
import './print.css'

let first = true

class CV extends Component {

  componentDidMount() {

    window.onafterprint = function(){
      window.location.href = "/curriculum"
    }

    document.title = `Giovanni Pregnolato Rosim - CV - ${this.props.current} `

    window.print()
  }

  printInScreen() {
    // 595 x 842

    const selector = 
    // ".body"
    "#print-cv"

    html2canvas(document.querySelector(selector)).then(canvas => {

      !first && document.body.removeChild(document.body.childNodes[0]);  
      first = false

      document.body.insertBefore(canvas, document.body.childNodes[0]);  // if you want see your screenshot in body.
      const imgData = canvas.toDataURL('image/png');
      // const pdf = new jsPDF();
      // pdf.addImage(imgData, 'PNG', 0, 0);
      // pdf.save("download.pdf"); 
  });
  }

  printInPDFByHTML() {
    const selector = 
    // ".body"
    "#print-cv"

    var pdf = new jsPDF('p', 'pt', 'a4');

    var specialElementHandlers = {
      // element with id of "bypass" - jQuery style selector
      // [selector]: function (element, renderer) {
      //     // true = "handled elsewhere, bypass text extraction"
      //     return true
      // }
  };
  var margins = {
      top: 80,
      bottom: 60,
      left: 40,
      width: 522
  };

    pdf.fromHTML(
      document.querySelector(selector),
      0,
      0,
      {
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
      },
      function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
      }, 
      margins
    )

  }

  printInPDF() {
    // 595 x 842

    const selector = 
    "#print-cv"

    html2canvas(document.querySelector(selector)).then(canvas => {
        // document.body.insertBefore(canvas, document.body.childNodes[0]);  // if you want see your screenshot in body.
        const imgData = canvas.toDataURL('image/png');
        console.log(canvas, canvas.width,canvas.height)
        // const pdf = new jsPDF();
        const pdf = new jsPDF('p', 'pt', 'a4');
        pdf.addImage(imgData, 'PNG', 0, 0, 
        
        canvas.width, canvas.height
        
        );

        pdf.addPage()

        pdf.addImage(imgData, 'PNG', 0, -842, 
        
        canvas.width, canvas.height
        );
        
        pdf.save("cv.pdf"); 
      });
    }

  render() {

    const {current} = this.props

    const {tabs} = config(current)

    return (
    <Style id="print-cv">
      {/* <button onClick={() => this.printInScreen()}>Print In Screen</button>
      <button onClick={() => this.printInPDF()}>Print In PDF</button> */}
      <div>
        <div className="title main-title">{getValidLanguageTxt(configCV.name, current)}</div>
        <div className="description">{getValidLanguageTxt(configCV.main_desc, current)}</div>
        <div className="description">{getValidLanguageTxt(configCV.address, current)}</div>
        <div className="description">{getValidLanguageTxt(configCV.contacts, current)}</div>
        
        <br/>

        <div className="description">{getValidLanguageTxt(configCV.portfolio, current)}</div>

        <br/>

        {
          tabs && tabs.map(item => {
            const {id, title, TabComponent, params} = item

            return (
              <div key={id} className={`cv-block ${id}`}>

                <div className="title cv-title">{title}</div>
                <hr />
                <div className="cv-content description"><TabComponent {...params} /></div>

                <br />
              </div>
            )
            
          })
        }
        </div>
    </Style>
    )
  }
}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CV)
