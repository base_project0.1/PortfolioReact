import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect, advancedConnect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
// import Style from './style'

import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

class Companies extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    const {current, profissional_experience, } = this.props

    return (
        <div className="companies">
          {
            profissional_experience && profissional_experience.map((item, id) => {
              const {name, period, job, activities, links, photoUrl} = item

              return (
                <div key={id} className="companie-card">
                  <Grid fluid>
                    <Row>
                      <Col xs={12} sm={2} md={2} lg={2} className="company-img">
                        <div className="companie-img-small"><img src={photoUrl}/></div>
                      </Col>
                      <Col xs={12} sm={10} md={10} lg={10}>
                        <div className="companie-tile">{getValidLanguageTxt(name, current)} &#160; {period.init} - {period.end}</div>
                        <div className="companie-job">{getValidLanguageTxt(job, current)}</div>
                        <div className="job-activities">{getValidLanguageTxt(activities, current)}</div>
                        <div className="company-projects">
                        {
                          links && links.map((l, id) => 
                            <a key={id} className="link-project" href={getValidLanguageTxt(l.url, current)} target="_blank">
                              {getValidLanguageTxt(l.desc, current)}
                            </a>  
                          )
                        }
                        </div>
                      </Col>
                    </Row>
                  </Grid>
                </div>
              )
            })
          }
        </div>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Companies)
