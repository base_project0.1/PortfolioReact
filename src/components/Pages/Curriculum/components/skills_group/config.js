export default [
  {
    "id": 2,
    "skill_name": "Javascript",
    "score": 9
  },
  {
    "id": 3,
    "skill_name": "HTML5",
    "score": 9
  },
  {
    "id": 1,
    "skill_name": "React",
    "score": 8.5
  },
  {
    "id": 4,
    "skill_name": "CSS3",
    "score": 8
  },
  {
    "id": 7,
    "skill_name": "C# (Unity 3D)",
    "score": 7.5
  },
  {
    "id": 8,
    "skill_name": "PHP",
    "score": 7
  },
  {
    "id": 6,
    "skill_name": "Node.js",
    "score": 6
  },
  {
    "id": 9,
    "skill_name": "SQL",
    "score": 5.5
  },
  {
    "id": 5,
    "skill_name": "React-Native",
    "score": 4
  },
]