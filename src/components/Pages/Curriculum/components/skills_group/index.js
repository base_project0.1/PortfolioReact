import React, {Component} from 'react'
import Style from './style'

import config from './config.js'

import ProgressBar from 'API_ComponentsBase/ProgressBar'

export default class SkillGroup extends Component {

  render() {
    return (
      <Style>
        {
          config.map(skill => 
            <ProgressBar 
              key={skill.id} 
              text={skill.skill_name} 
              value={skill.score} /> 
          )
        }
      </Style>
    )
  }

}