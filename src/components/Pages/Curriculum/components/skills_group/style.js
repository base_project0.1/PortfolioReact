import styled from 'styled-components'

export default styled.div `


  .progress-bar {
    background-color: grey;
    border-radius: 5px;
    margin: 7px 0 7px 0;
    z-index: 0;
  }

  .bar-inside-name {
    background-color: #515151;
    border-radius: 5px;
    z-index: 11;
    padding: 10px;
  }

  .bar-inside-value {
    background-color: #757575;
    border-radius: 5px;
    width: 70%;
    z-index: 10;
    padding: 10px;
    text-align: right;
    min-width: 146px;

    * {
      width: 100%;
    }
  }


`