import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Link } from "react-router-dom";

//API & Reducers
import { connect, advancedConnect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import Style from './style'

//Configs
import config from './config/config.js'

//Utils

//Components
import Collapse from 'API_ComponentsBase/Collapse/SimpleCollapse'

import TweenMax from 'gsap'


class Curriculum extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {

    TweenMax.to('.btn-download-cv div', 0, {autoAlpha: 0, top: '+=200', position: 'relative'})

    TweenMax.staggerFromTo(
      '.collapse-group > div', 
      1,
      {top: '-=200', opacity: 0, position: 'relative'},
      {top: '+=200', opacity: 1, ease: Circ.easeOut},
      0.3,
      () => {
        TweenMax.to('.btn-download-cv div', 1, {autoAlpha: 1, top: '-=200', position: 'relative'})
      }
    )

  }

  render() {

    const {current} = this.props

    const {tabs, cv_download} = config(current)

    return (
      <Style>
        <div className="curriculum-page" style={{color:'white'}}>
          <div className="collapse-group">
            {
              tabs && tabs.map((item, id) => {
                const {title, TabComponent, params} = item
                return (
                  <Collapse key={id}
                    header={<div className="title header-tab">{title}</div>}
                    content={<div className="content-tab"><TabComponent {...params}/></div>}
                  />
                )
              })
            }
          </div>
          <a className="btn-download-cv" href={cv_download.href} target="_blank">
            <div>{cv_download.label}</div>
          </a>
        </div>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Curriculum)
