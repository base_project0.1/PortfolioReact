import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './footerStyle'

import config from './config/footerConfig'

import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'
//
// import Loading from 'components/Loading'

//http://www.redbitdev.com/getting-started-with-react-flexbox-grid/
// lg – 75em (most often 1200px) and up
//
// md – 64em (most often 1024px) to 74em
//
// sm – 48em (most often 768px) to 63em
//
// xs – up to 47em

import TweenMax from 'gsap'

class Footer extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {

    let divMainHeader = document.querySelector(".main-footer").parentElement

    TweenMax.from(
      divMainHeader, 2, {
        bottom: '-25', delay: 1, ease: Elastic.easeOut,
      })

  }

  render() {

    const {current} = this.props

    return (
      <Style className="fixed">
        <Grid fluid className="main-footer">
          <Row>
            <Col xs={12} sm={12} md={12} lg={12}>
              <label>..:: Giovanni Pregnolato Rosim ::..</label>
              <label className="subtitle">
                <i className="fa fa-gamepad" />
                &nbsp; {getValidLanguageTxt(config.subtitle, current)} &nbsp;
                <i className="fa fa-gamepad" />
              </label>
            </Col>
          </Row>
        </Grid>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer)
