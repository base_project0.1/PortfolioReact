import styled from 'styled-components'
import { height } from 'utils/header.js'

const style = styled.div`

  position: fixed;
  top: 0px;
  left: 0px;
  height: ${height}px;
  width: 100%;

  display: inline-block;

  background: linear-gradient(to right, #212121 , #676767, #212121 );
  border: 1px solid #00000040;

  z-index:10;

  -webkit-touch-callout: none;  /* iPhone OS, Safari */
  -webkit-user-select: none;    /* Chrome, Safari 3 */
  -khtml-user-select: none;     /* Safari 2 */
  -moz-user-select: none;       /* Firefox */
  -ms-user-select: none;        /* IE10+ */
  user-select: none;            /* Possível implementação no futuro */

  .languages{

    background-color: white;
    padding: 0 3px;
    border-radius: 0px 5px 5px 0px;
    border: 1px solid #888888;
    float: left;

    .headerDropDown {
      cursor: pointer;
      line-height: ${height - 6}px;

      ${'' /* .expandIcon {margin: 0px 3px;}
      label {font-size: 0.9em;}
      .flagIcon {height: 10px; width: 10px;} */}
    }

    .itemDropDown {
      margin: 1px 0px;
      cursor: pointer;
      label { vertical-align: middle; cursor: pointer;}
      height: ${height}px;
      line-height: ${height}px;
      .flagIcon {
        cursor: pointer;
        width: 16px;
        height: 16px;
        margin-right: 3px;
        vertical-align: middle;
      }
    }
    .itemDropDown.selectedItem {
      background: linear-gradient(#bbbbbb, #efefefbb, #bbbbbb);
    }

    .itemDropDown:hover{
      background: linear-gradient(#8181ff, #b2b2ff, #8181ff);
    }
    hr {
      margin: 0px;
      margin-top: 3px;
    }
  }

  .mainLinks {
    float: right;
    display: inline-flex;
    input { outline: 0 none; }

    .btn-mainLinks {
      .mainDiv {
        height: ${height}px;
        width: ${height}px;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
      }
    }

    .btn-mainLinks i {
      transition: .5s;
    }
    .btn-mainLinks:hover i {
      color: #d6d6d6;
      text-shadow: 2px 2px #000000aa;
    }

    .linkDesc {
      // display: initial;
      position: absolute;
      top: ${height}px;
      right: 0px;
      width: 145px;
      text-align: center;
      background-color: black;
      color: white;
      border-radius: 5px 0px 0px 5px;
      font-size: .75em;
      padding: 3px 3px;
      border: 1px solid #b9b9b957;
      z-index: -1;
    }
    .linkDesc a, .linkDesc a:visited {
      color: white !important;
    }

    .hidden-input {
      height: 0px;
      opacity: 0;
      position: absolute;
    }

    .help-icon i {
      position: absolute;
      top: 21px;
      right: -5px;
      font-size: 20px;
      border: 1px solid #b9b9b957;
      border-radius: 3px;
      padding: 3px;
      background-color: black;
    }

    i{
      margin: 0px 3px;
      cursor: pointer;
      color white;
      font-size: 22px;
    }
    .mainDiv.selected {
      background: black;
      border-radius: 5px 5px 0px 0px;
      border: 1px solid #b9b9b957;
    }
  }

  .expandIcon, .lblLanguage, .flagIcon-min{
    vertical-align: super;
    cursor: pointer;
  }
  .lblLanguage {font-size: 0.9em; margin: 0 5px;}
  .flagIcon-min {height: 10px; width: 10px;}


  .home-button {

    top: 36px;
    position: absolute;
    text-align: center;
    width: 100%;
    z-index: -1;

    pointer-events: none;

    i {
      pointer-events: all;
      cursor: pointer;
      color: white;
      font-size: 30px;
      padding: 5px;
      background: linear-gradient(to right, #4a4a4a , #676767, #4a4a4a );
      border-radius: 0px 0px 50% 50%;
      transition: 1s;
    }

    i.disabled {
      pointer-events: none;
      visibility: hidden;
    }

  }

  .home-button:hover {
    i:not(.disabled) {
      font-size: 34px;
      color: #d6d6d6;
      text-shadow: 2px 2px #000000b5;
    }
  }
`
export default style
