export default [
  {
    icon: 'whatsapp',
    text: '+55 (11) 99559-3220',
    title: 'Phone'
  },
  {
    icon: 'envelope',
    text: 'giovanni_pr@hotmail.com',
    title: 'Email'
  },
  // {
  //   icon: 'facebook-square',
  //   link: 'https://www.facebook.com/giovanni.p.rosim',
  //   title: 'Facebook'
  // },
  {
    icon: 'linkedin',
    text: 'giovanni-pregnolato-rosim',
    link: 'https://www.linkedin.com/in/giovanni-pregnolato-rosim-7b709211a/',
    title: 'Linkedin'
  },
  // {
  //   icon: 'skype',
  //   text: 'giovanni_pr1',
  //   title: 'Skype'
  // },
  {
    icon: 'gitlab',
    text: 'giovanniprosim',
    link: 'https://gitlab.com/giovanniprosim',
    title: 'GitLab',
  },
  // {
  //   icon: 'code',
  //   link: 'https://www.hackerrank.com/giovanniprosim?hr_r=1',
  //   title: 'Hacker Rank'
  // },
]
