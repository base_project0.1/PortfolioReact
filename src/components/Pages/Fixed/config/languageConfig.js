import iconBR from 'images/flags/pt-br.svg'
import iconEN from 'images/flags/en-us.svg'

export default [
    {
      id: 'ptBR',
      icon: iconBR,
      name: 'Portuguese',
      label: {
        default: 'Portuguese',
        ptBR: 'Português',
        enEN: 'Portuguese'
      }
    },
    {
      id: 'enEN',
      icon: iconEN,
      name: 'English',
      label: {
        default: 'English',
        ptBR: 'Inglês',
        enEN: 'English'
      }
    }
]
  