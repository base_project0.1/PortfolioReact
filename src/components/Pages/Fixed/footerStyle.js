

import styled from 'styled-components'

import { height } from 'utils/footer'

const style = styled.div`

  position: fixed;
  bottom: 0px;
  left: 0px;
  width: 100%;
  height: ${height}px;

  background: linear-gradient(to right, #212121 , #676767, #212121 );
  border: 1px solid #00000040;

  z-index:10;

  padding: 1px !important;
  text-align: center;

  label {
    font-size: 13px;
    letter-spacing: 0.25em;
    color: white;
    text-align: center;
    display: block;
  }

  .subtitle {
    line-height: ${height/2}px;
    @media screen and (max-width: 375px) {
        font-size: 9px;
      }
  }
`
export default style
