import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './headerStyle'

// import Loading from 'components/Loading'

import { Link } from "react-router-dom";

// Utils
import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'
import LOCAL_STORAGE from 'API_Utils/localStorage.js'
import headerUtils from 'utils/header.js'

import routes from '../../../routes/routes'

import linksConfig from './config/linksConfig.js'
import languageConfig from './config/languageConfig.js'

import 'fontAwesome'

import TweenMax from 'gsap'

class Header extends Component {

  constructor(props) {
    super(props)

    this.toggleTextOfIcon = this.toggleTextOfIcon.bind(this)
    this.toggleDropDown = this.toggleDropDown.bind(this)

    this.state = {
      languageItem: null,
      showDropDownItens: false,
      isHome: false
    }

    this.linksDesc = []

    this.props.actions.setLanguage()
  }

  toggleTextOfIcon(name) {
    const allLinksDesc = this.linksDesc
    let cInput = allLinksDesc[name]
    let cDiv = document.querySelector('.'+name+'.mainDiv')

    if(cDiv.className.indexOf('selected') < 0){
            
      Object.keys(this.linksDesc).forEach(name => {
        document.querySelector('.'+name+'.mainDiv').className = name+' mainDiv'
        this.linksDesc[name].style.display = 'none'
      })

      cDiv.className += ' selected'

      cInput.style.opacity = 1
      cInput.style.right = 0
      cInput.style.display = 'initial'
      if(this.linksAnimation) this.linksAnimation.kill()
      this.linksAnimation = TweenMax.from(
        cInput, 1, {
          right: '-250', ease: Circ.easeOut, position: 'absolute'
      })
    }
    else{
      if(this.linksAnimation) this.linksAnimation.kill()
      this.linksAnimation = TweenMax.to(
        cInput, 1, {
          right: '-250', ease: Circ.easeOut, position: 'absolute', opacity: 0,
          onComplete: () => {
            cInput.style.display = 'none'
          }
        })
        cDiv.className = cDiv.className.replace(/ selected/g, '')
    }
  }

  configLanguages() {
    return languageConfig
  }

  copyTextToClipboard(selector) {
    const copyText = document.querySelector(selector);

    /* Select the text field */
    copyText.select(); 
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  
    /* Copy the text inside the text field */
    document.execCommand("copy");
  
    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
  }

  createMainLink(config) {
    return (
      <div className="mainLinks">
        {
          config.map( (item, id) => {

            const iconConfig = {
              title: item.title,
              className: "fa fa-".concat(item.icon),
              onClick: item.text && (() => { this.toggleTextOfIcon("inputMainLinks"+id) }),
            }
            const icon = (
              <div className={"inputMainLinks"+id+" mainDiv"} onClick={iconConfig.onClick}>
                <i className={iconConfig.className} title={iconConfig.title} />
              </div>
            )
            
            const iconHelpConfig = item.link ?
            { 
              className: "fa fa-link",
              onClick: (() => {window.open(item.link, '_blank');}),
            }
            :
            {
              className: "fa fa-copy",
              onClick: (() => {this.copyTextToClipboard("input."+item.title)}),
            }
            const iconHelp = <div className="help-icon"><i {...iconHelpConfig} /></div>
            
            const textElement = 
              item.link ?
              <a href={item.link} target="_blank">{item.text}</a>
              :
              <span onClick={iconHelpConfig.onClick}>
                <input className={"hidden-input " + item.title} defaultValue={item.text} />
                {item.text}
              </span>
            const component = 
              <div>
                <div className={"inputMainLinks"+id+" linkDesc"}
                  ref={input => this.linksDesc["inputMainLinks"+id] = input}
                  style={{display:'none'}}
                >
                  {textElement}
                  {iconHelp}
                </div>
                {icon}
              </div>
            return <div key={id} className={"btn-mainLinks " + item.title}>{component}</div>
          })
        }
      </div>
    )
  }

  /* DropDown Languagens (init) */
  createLanguageDropDown(config) {
    const {languageItem, showDropDownItens} = this.state
    const itemConfig = languageItem || config[0]

    return (
      <div className="languages">
        <div className="headerDropDown" onClick={this.toggleDropDown}>
          <img src={itemConfig.icon} className="flagIcon-min" />
          <label className="lblLanguage">{getValidLanguageTxt(itemConfig.label, this.props.current)}</label>
          <i className="fa fa-angle-down expandIcon" />
        </div>
        {showDropDownItens && <hr style={{border: '1px solid black'}}/>}
        {
          showDropDownItens && config.map( (item, id) =>
            <div key={id}
              className={["itemDropDown", 
                          this.props.current == item.id ? 'selectedItem' : null].join(" ")}
              onClick={() => this.choiceItemDropDown(item)}>
              {id > 0 && <hr/>}
              <img src={item.icon} className="flagIcon" />
              <label>{getValidLanguageTxt(item.label, this.props.current)}</label>
            </div>
          )
        }
      </div>
    )
  }

  choiceItemDropDown(item) {
    this.toggleDropDown()
    this.setLanguageDropDown(item)
  }

  setLanguageDropDown(item) {
    this.setState({languageItem: item})
    this.props.actions.setLanguage(item.id)
    LOCAL_STORAGE.set('currentLanguage', item.id)
  }

  toggleDropDown(){
    this.setState({showDropDownItens: !this.state.showDropDownItens})
  }
  /* DropDown Languagens (end) */

  /* Language Functions (init) */
  getLanguageItemById(id) {
    return this.configLanguages().find(item => item.id === id)
  }
  /* Language Functions (end) */

  componentWillMount() {
    this.setLanguageDefault()
  }

  componentDidMount() {
    this.animationHeader()
  }

  setLanguageDefault() {
    
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    const urlLanguage = urlParams.get('l')

    const currentLanguage = urlLanguage || LOCAL_STORAGE.get('currentLanguage') || this.props.current

    const currentLanguageItem = languageConfig.find(
      item => 
      currentLanguage.toLowerCase() === item.id.toLowerCase()
    )
    if(currentLanguageItem) this.setLanguageDropDown(currentLanguageItem)
  }

  animationHeader() {

    let body = document.querySelectorAll(".body")

    let divMainHeader = document.querySelector(".main-header").parentElement

    TweenMax.to('.btn-mainLinks', 0, {opacity: 0})

    TweenMax.fromTo('.home-button', .5, 
    {scale: 0, opacity: 0}, 
    {scale: 1, opacity: 1, ease: Circ.easeOut, delay: 2})

    TweenMax.from(
      divMainHeader, 2, {
        top: '-40', delay: 1, ease: Elastic.easeOut,
        onComplete: () => {
          TweenMax.staggerFromTo(
            '.btn-mainLinks', 
            0.3,
            {opacity: 0, backgroundColor: '#ffffff4a'},
            {opacity: 1, backgroundColor: 'none'},
            0.15,
            // /*onCompleteAll:*/ () => body.forEach(s => s.classList.remove('disable'))
          )
        }
      })
  }

  render() {
    return (
      <Style className="fixed">
        <div className="main-header">
          {/* TODO: ptBR is a default language now, because I need to improve the translation */}
          {this.createLanguageDropDown(this.configLanguages())}
          {this.createMainLink(linksConfig)}
          <div className="home-button">
            <Link to='/'>
              <i title="Home" className={'fa fa-home ' + (this.state.isHome ? 'disabled' : '')} />
            </Link>
          </div>
        </div>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
