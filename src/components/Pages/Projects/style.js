import styled from 'styled-components'

const style = styled.div`

    min-height: calc(100vh - 50px);
    width: 100%;
    padding-top: 50px !important;
    padding-bottom: 5px !important;
    background: repeating-linear-gradient(#6d6d6d, grey, #d0d0d0, grey, #6d6d6d);

    text-decoration: none;
    color: unset;

    .filter-container {
        background-color: #373737;
        padding: 25px 15px;
        margin-top: -25px;
    }

    .be-the-hero {
        .mainImg img {
            height: auto !important;
            max-width: 100% !important;
        }
    }

`
export default style
