import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { BrowserRouter, Router, Route, Link } from "react-router-dom"

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

//Components
import Card from './components/card'
import CardDetail from './components/card_detail'
import Filter from './components/filter'
import configFilter from './components/filter/config'

import no_img from 'images/no_img.png'

//Cards Detail
import wizards_potion_detail from './components/card_detail/configs/wizards_potion.js'
import shock_sheep_detail from './components/card_detail/configs/shock_sheep.js'
import math_athlete_detail from './components/card_detail/configs/math_athlete.js'
import christmas_gift_detail from './components/card_detail/configs/christmas_gift.js'
import abrindo_caminho_detail from './components/card_detail/configs/abrindo_caminho.js'
import garbage_baseball_detail from './components/card_detail/configs/garbage_baseball.js'
import squeezing_acne_detail from './components/card_detail/configs/squeezing_acne.js'
import profuturo_detail from './components/card_detail/configs/profuturo.js'
import dungeon_escape_detail from './components/card_detail/configs/dungeon_escape.js'
import tcc_ai_detail from './components/card_detail/configs/tcc_ai.js'
import cet2018_detail from './components/card_detail/configs/cet2018.js'
import math_in_games01_detail from './components/card_detail/configs/math_in_games01.js'
import odontoprev_benef_detail from './components/card_detail/configs/odontoprev_benef.js'
import toyota_dds_detail from './components/card_detail/configs/toyota_dds.js'
import sulamerica_prest_detail from './components/card_detail/configs/sulamerica_prest.js'
import ted_projects_detail from './components/card_detail/configs/ted_projects.js'
import ambev_detail from './components/card_detail/configs/ambev.js'
import natal_just_2020_detail from './components/card_detail/configs/natal_just_2020.js'
import natura_chronos_detail from './components/card_detail/configs/natura_chronos.js'
import be_the_hero_detail from './components/card_detail/configs/be_the_hero.js'
import fresco_wp_detail from './components/card_detail/configs/fresco_wp.js'
import ecoleta_detail from './components/card_detail/configs/ecoleta.js'
import leao_e_o_rato_detail from './components/card_detail/configs/leao_e_o_rato.js'
import chefe_gio from './components/card_detail/configs/chefe-gio.js'
import proffy from './components/card_detail/configs/proffy.js'
import gdev from './components/card_detail/configs/gdev.js'
import covid_sim from './components/card_detail/configs/covid-19-simulation.js'
import pandemusic from './components/card_detail/configs/pandemusic.js'
import progAmando from './components/card_detail/configs/progAmando.js'
import happy from './components/card_detail/configs/happy.js'
import traders_club_CRUD_detail from './components/card_detail/configs/traders_club_CRUD.json'

// import Pagination from 'components/Pagination'
// import Loading from 'components/Loading'

import TweenMax from 'gsap'

import routes from '../../../routes/routes'

// import 'fontAwesome'

// Utils
import urlParams from 'API_Utils/urlParams.js'

class Projects extends Component {

  constructor(props) {
    super(props)

    this.changeFilter = this.changeFilter.bind(this)

    this.allCardConfig = this.configCards()
    this.filterCardsConfig = {}
    this.filterTypes = Object.keys(configFilter)

    this.setCardFilter(this.allCardConfig)

    this.urlParams = new urlParams(window.location.href)

    this.state = {
      currentFilter: 'main'
    }
  }

  configCards() {
    return [
      toyota_dds_detail,
      odontoprev_benef_detail,
      sulamerica_prest_detail,
      ted_projects_detail,
      natura_chronos_detail,
      chefe_gio,
      progAmando,
      natal_just_2020_detail,
      covid_sim,
      ambev_detail,
      wizards_potion_detail,
      shock_sheep_detail,
      dungeon_escape_detail,
      math_in_games01_detail,
      profuturo_detail,
      leao_e_o_rato_detail,
      gdev,
      math_athlete_detail,
      tcc_ai_detail,
      cet2018_detail,
      christmas_gift_detail,
      abrindo_caminho_detail,
      pandemusic,
      fresco_wp_detail,
      happy,
      proffy,
      be_the_hero_detail,
      ecoleta_detail,
      garbage_baseball_detail,
      squeezing_acne_detail,
      // traders_club_CRUD_detail
    ]
  }

  setCardFilter(allConfigs) {
    allConfigs.forEach( (config, id) => {
      config.tags.forEach(tag => {
        if(!this.filterCardsConfig[tag]) this.filterCardsConfig[tag] = []
        this.filterCardsConfig[tag].push(config)
      })
    })
  }

  createCardsList(config) {
      return (
        <Grid fluid>
          <Row>
            {
              config && config.map((item, id) =>
                <Col className={"project-card " + item.link.substr(1)} key={item.link+'-'+this.state.currentFilter} xs={12} sm={4} md={4} lg={3}>
                  <Card {...item} default_img={no_img}/>
                </Col>
              )
            }
          </Row>
        </Grid>
      )
  }

  componentDidMount() {
    const urlFilter = this.urlParams.get()['f']

    if(urlFilter && urlFilter != this.state.currentFilter) {
      this.setState({currentFilter: urlFilter}, () => this.cardStartAnimation())
      return
    }

    this.cardStartAnimation()

  }

  cardStartAnimation() {
    this.cardsEntranceAnimation()

    TweenMax.fromTo(
      '.filter, .current-filter', 
      1,
      {top: '-=200', opacity: 0, position: 'relative'},
      {top: '+=200', opacity: 1, ease: Circ.easeOut},
      0.3
    )
  }

  cardsResetAnimation() {
    this.currentCardAnimation && this.currentCardAnimation.kill && this.currentCardAnimation.kill()
  }

  cardsEntranceAnimation() {

    this.cardsResetAnimation()

    this.currentCardAnimation = TweenMax.staggerFromTo(
      '.project-card', 
      1,
      {left: '-=200', opacity: 0, position: 'relative'},
      {left: '0', opacity: 1, ease: Circ.easeOut},
      0.3,
    )
  }

  changeFilter(nextFilter) {
    this.urlParams.set({f: nextFilter})
    this.setState({currentFilter: nextFilter}, () => this.cardsEntranceAnimation())
  }

  onCloseModal() {
    this.props.history.push(routes['Projects'] + '/' + window.location.search );
  }

  render() {

    const { currentFilter } = this.state

    return (
      <div>
        <Style>
          <Filter types={this.filterTypes} onChange={this.changeFilter} currentFilter={currentFilter} />
          <Router history={this.props.history}>
            <div>
              {
                this.configCards().map((card, id) =>
                  <Route key={id}
                    path={routes.Projects.concat(card.link)}
                    render={() => <CardDetail id={card.link.replace("/", "")} {...card } onClose={() => this.onCloseModal()} default_img={no_img} />}
                  />
                )
              }
            </div>
          </Router>
          <div className="filter-container" style={{color:'white'}}>
            {this.createCardsList(this.filterCardsConfig[currentFilter])}
          </div>
        </Style>
      </div>

    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects)
