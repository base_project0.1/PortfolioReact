import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

import TweenMax from 'gsap'

import config from './config'

import 'fontAwesome'

//Utils
import {getValidLanguageTxt} from 'API_Utils/languageVerify.js'

class Filter extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    const { types, currentFilter, onChange, current } = this.props

    return (
      <Style>
        <div className='filter'>
            {
              types.map((type, id) => 
                <div
                  key={id}
                  className={`filter-type ${currentFilter === type && 'active'}`}
                  onClick={ ()=> onChange(type) }
                  title={getValidLanguageTxt(config[type].text, current)}
                >
                  <i className={`fa fa-${config[type].icon}`} />
                </div>
              )
            }
        </div>
        <div className='current-filter'>{ getValidLanguageTxt(config[currentFilter].text, current) }</div>
      </Style>

    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)
