export default {

    main: {
        text: {
            "default": "main",
            "ptBR": "principais",
            "enEN": "main"
        },
        icon: "star",
    },

    web: {
        text: {
            "default": "web",
            "ptBR": "",
            "enEN": "web"
        },
        icon: "globe"
    },

    game: {
        text: {
            "default": "games",
            "ptBR": "jogos",
            "enEN": "games"
        },
        icon: "gamepad",
    },

    presentation: {
        text: {
            "default": "presentation",
            "ptBR": "apresentação",
            "enEN": "presentationes"
        },
        icon: "comments",
    },

    other: {
        text: {
            "default": "others",
            "ptBR": "outros",
            "enEN": "others"
        },
        icon: "ellipsis-h",
    }
    
}