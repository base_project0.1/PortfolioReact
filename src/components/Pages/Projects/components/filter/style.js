import styled from 'styled-components'

const style = styled.div`

    .filter {
        display: inline-flex;
        justify-content: space-around;
        width: 100%;

        .filter-type{
            border-radius: 10px 10px 0 0;
            width: 100%;
            text-align: center;
            border: 1px solid gray;
            border-bottom: none;
            background-color: #b7b7b7;
            cursor: pointer;
            top: 1px;
            position: relative;

            transition: .3s ease;

            i {
                font-size: 2em;
                color: #a7a7a7; 
                padding: 3px;
            }

            &.active {
                background-color: #373737;
                cursor: auto;

                i {
                    color: whitesmoke;
                }
            }
        }

        .filter-type:not(.active):hover {
            box-shadow: 0px -2px 5px 0px #00000061;
            border-bottom: none;
            width: 110%;
            i {
                transform: scale(1.1);
                color: #373737;
            }
        }
    }

    .current-filter {
        text-align: center;
        margin-bottom: 10px;
        color: whitesmoke;
        background-color: #373737;
        padding: 5px;
        text-transform: uppercase;
    }

`
export default style
