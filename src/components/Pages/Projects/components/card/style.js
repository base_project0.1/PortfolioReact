import styled from 'styled-components'

const style = styled.div`

  cursor:pointer;

  overflow: hidden;
  border: 1px solid white;
  margin: 5px !important;
  background-color: blue;

  a {
    text-decoration: none;
    color: unset;
    outline: none;
  }

  .mainCard {
    padding: 0px !important;
    .mainTitle {
      text-align: center;
      font-size: small;
      font-weight: bolder;
      letter-spacing: .25em;
      text-transform: uppercase;

      height: 30px;
      padding: 5px 10px;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .mainImg {
      position: relative;
      overflow: hidden;
      height: 150px;
      border: 1px solid;
      background: linear-gradient(#9e9e9e8c, #e0e0e08c, #9e9e9e8c);
      img {
        position: relative;
        height: 100%;
        transform: translate(-50%,-50%);
        transition: transform 1.5s;
        top: 50%;
        left: 50%;
      }
    }

    .mainImg:hover img{
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%) scale(1.25);
    }

    // .mainDesc {
    //   margin: 3px !important;
    //   padding: 10px !important;
    //   border-radius: 10%;
    //   background: linear-gradient(#9e9e9e8c, #e0e0e08c, #9e9e9e8c);
    //   position: relative;
    //   height: 106px;
    //   overflow: hidden;
    //   border: 1px solid;
    //   text-overflow: ellipsis;
    //   height: 106px;

    //   display: -webkit-box;
    //   -webkit-line-clamp: 6;
    //   -webkit-box-orient: vertical;
    //   label {
    //     display: inline-block;
    //     width: 100%;
    //     height: 100%;
    //   }
    //   // display:none;
    // }
  }

`
export default style
