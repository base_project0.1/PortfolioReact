import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

import CardDetail from '../card_detail'

// import Pagination from 'components/Pagination'
//
// import Loading from 'components/Loading'

// import 'fontAwesome'

import routes from '../../../../../routes/routes'

import LggeFct, {getValidLanguageTxt} from 'API_Utils/languageVerify.js'
import { BrowserRouter, Route, Link, Redirect } from "react-router-dom"

class Card extends Component {

  constructor(props) {
    super(props)

    this.state = {
      showModal: false
    }

    this.toggleDetailProjectModal = this.toggleDetailProjectModal.bind(this)
    this.closeDetailProjectModal = this.closeDetailProjectModal.bind(this)
  }

  closeDetailProjectModal() {
    this.setState({showModal: false})
  }

  toggleDetailProjectModal() {
    this.setState({showModal: !this.state.showModal})
  }

  render() {

    const {current, default_img} = this.props

    const {title, thumbnail, desc, link, styleContent, tags :[tag]} = this.props

    return (
      <Style style={styleContent}>
        <Link to={routes.Projects+link/*+`\?f=${tag}`*/ + window.location.search} onClick={this.toggleDetailProjectModal}>
          <div className="mainCard">
            <div className="mainTitle">
              {getValidLanguageTxt(title, current)}
            </div>

            <div className="mainImg" style={{
              background: 'linear-gradient(to bottom, rgba(255,255,255,0.7) 0%,rgba(255,255,255,0.7) 100%), url('+getValidLanguageTxt(thumbnail, current) +') repeat 0 0',
              backgroundSize: 'cover',
            }}>
              <img src={getValidLanguageTxt(thumbnail, current) || default_img} />
            </div>

            {/* <div className="mainDesc">
              {getValidLanguageTxt(desc, current)}
            </div> */}
          </div>
          {/* {
            this.state.showModal &&
            <CardDetail {...this.props.cardDetail}
              onClose={this.closeDetailProjectModal}
            />
          } */}
        </Link>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

Card.propTypes = {
  title: PropTypes.object,
  thumbnail: PropTypes.object,
  desc: PropTypes.object,
}

export default connect(mapStateToProps, mapDispatchToProps)(Card)
