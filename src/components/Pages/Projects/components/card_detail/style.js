import styled from 'styled-components'

const border = '#ffffff2e 1px solid';

const style = styled.div`

  ${'' /* overflow: hidden !important; */}

  .cardTitle {
    text-transform: uppercase;
    text-align: center;
    font-size: larger;
    font-weight: bolder;
    background-color: #ffffff30;
    padding: 5px 15px;
    letter-spacing: 0.25em;
    border: ${border};
  }

  i {
    position: absolute;
    top: 0px;
    right: 0px;
    font-size: 1.5em;
    color: #cacacaad;
    ${'' /* background-color: #676767;
    border-radius: 50%; */}
    cursor: pointer;
    transition: all 1s ease;
  }

  i:hover {
    color: #969696;
    font-size: 1.6em;
  }

  .cardContent {
    height: 65px;
    overflow-y: auto;

    font-size: 18px;

    display: inline;
  }

  .cardDesc {
      background-color: #ffffff3d;
      padding: 20px;
      text-align: justify;
      margin-top: 5px;
  }

  .cardLink {
    margin-top: 8px;
    text-align: center;
    a {
      text-decoration: none;
      color: unset;
      outline: none;
      font-weight: bold;
    }
  }

  .slideShow {
    width: 100%;
    background-color: #0000006e;

    .slide-show {
      border: ${border};
    }

    li {
        background-size: contain;
    }

    .show-index {
      p{
        background-color: #00000033;
        border-radius: 5px;
        padding: 1px 5px 1px 5px;
        font-weight: bold;
        box-shadow: #000000ba 0px 0px 3px 1px;
      }
    }
  }

  @media screen and (max-width: 670px) {
    .medias {
      height: 35vh !important;
      &.toyota_dds, &.odontoprev_benef, &.ted, &.natura_chronos, &.natal_just_2020,
      &.ambev_comex, &.wizards_potion {
        height: 35vh !important;
      }  
    }
  }



`
export default style
