import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'
import ModalStyle from './modalStyle'
import './style.css'

import Modal from 'API_ComponentsBase/Modal/ReactModal'
// import Pagination from 'components/Pagination'
//
// import Loading from 'components/Loading'

import MySlideShow from 'API_ComponentsStyled/SlideShow/index'

import routes from '../../../../../routes/routes'

import 'fontAwesome'

import LggeFct, {
  getValidLanguageTxt, 
  getValidArrayImgs, 
  getValidArrayVideos
} from 'API_Utils/languageVerify.js'

class CardDetail extends Component {

  constructor(props) {
    super(props)
    this.show = true

    this.onClose = this.onClose.bind(this)
  }

  createLink(obj, currentLanguage) {
    return (
      <div>
        <a href={obj.url} target="_blank">
          {getValidLanguageTxt(obj.desc, currentLanguage)}
        </a>
      </div>
    )
  }

  onClose() {
    const {onClose} = this.props
    if(onClose) {onClose()}
    else {
      this.show = false
      this.forceUpdate()
    }
  }

  getImages() {

    const {current, images, default_img} = this.props

    return images ? (
      images.defaultPath ?
      (images.defaultPath + getValidArrayImgs(images, current).join(';' + images.defaultPath)).split(';')
      :
      getValidArrayImgs(images, current)
    ) : [default_img]

  }

  getVideos() {

    const {current, videos, default_img} = this.props

    return videos ? (
      videos.defaultPath ?
      (videos.defaultPath + getValidArrayVideos(videos, current).join(';' + videos.defaultPath)).split(';')
      :
      getValidArrayVideos(videos, current)
    ) : []

  }

  getMedias() {
    return [].concat(this.getVideos(), this.getImages())
  }

  render() {

  const {id, title, desc, images, link_download, link_promo, styleContent, styleDesc, styleOverlay, styleClose, current, default_img} = this.props

  return (
      <ModalStyle>
        <Modal
          style={{
              content: {
                maxWidth: '600px',
                position: 'relative',
                fontFamily: 'Lato',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                height: '70vh',
                width: '80%',
                // overflow: 'hidden !important',
                overflowY: 'auto',
                borderRadius: 'unset',
                boxShadow: 'black 0px 0px 20px 5px',
                paddingBottom: '10px',
                overflowX: 'hidden',
                ...styleContent
              },
              overlay: {
                zIndex: 10,
                ...styleOverlay,
              }
            }}
          className= "card-detail__modal"
          ariaHideApp={false}
          isOpen={this.show}
          shouldCloseOnEsc
          shouldCloseOnOverlayClick
          onRequestClose={this.onClose}
        >
          <Style>
          <div className="cardTitle">{getValidLanguageTxt(title, current)}</div>
          <div className="slideShow">
            <MySlideShow 
              medias={this.getMedias()}
              mediasClass={id}
            />
          </div>
          <div className="cardContent" style={{...styleDesc}}>
            <div className="cardDesc">{getValidLanguageTxt(desc, current)}</div>
            {link_promo && <div className="cardLink">{this.createLink(link_promo, current)}</div>}
            {link_download && <div className="cardLink">{this.createLink(link_download, current)}</div>}
          </div>
          <i className="fa fa-times-circle" onClick={this.onClose} style={{...styleClose}}/>
          </Style>
        </Modal>
      </ModalStyle>
    )
  }
}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

CardDetail.propTypes = {
  // title: PropTypes.obj,
  // imgURL: PropTypes.obj,
  // desc: PropTypes.obj,
}

export default connect(mapStateToProps, mapDispatchToProps)(CardDetail)
