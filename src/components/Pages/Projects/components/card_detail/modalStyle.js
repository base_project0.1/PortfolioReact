import styled from 'styled-components'

const modalStyle = styled.div`

${'' /* ReactModal__Content ReactModal__Content--after-open */}

  color:white;
  padding 50px;
  

  .ReactModal__Content--after-open {
    color:white;
  }

  div::-webkit-scrollbar {
    width: 1em;
  }

  div::-webkit-scrollbar-track {
    background: #00000052;
  }

  div::-webkit-scrollbar-thumb {
    background-color: #a2a2a23b;
    border: 1px solid #00000033;
    border-radius: 5px;
  }

`
export default modalStyle
