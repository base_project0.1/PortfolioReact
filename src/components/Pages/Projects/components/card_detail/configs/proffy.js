import logo from 'images/projects/Proffy/p_logo.png'

import img1 from 'images/projects/Proffy/p01.png'
import img2 from 'images/projects/Proffy/p02.png'

export default {
  "title":{
    "default":"Proffy",
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "App frontend and backend develop in a course of Next Level Week 2.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks and Expo.",
    "ptBR": "Feito para a semana Next Level Week 2.0 da Rockseat, utilizando <u>React.js</u>, <u>React Native</u> e <u>Node.js</u>. Além de aprender a utilizar o TypeScript, React Hooks e o Expo.",
    "enEN": "App frontend and backend develop in a course of Next Level Week 2.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks and Expo."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link": "/proffy",

  tags: ["other"],

  "styleContent": {
    "backgroundColor": "#8257e5",
    "color": "white"
  }
}
