import img1 from 'images/projects/TCC AI/situacao perfeita3.png'
import img2 from 'images/projects/TCC AI/situacao perfeita4.png'
import img3 from 'images/projects/TCC AI/situacao perfeita5.png'

export default {
  "title":{
    "default":"TCC AI",
    "ptBR": "TCC IA",
    "enEN": "TCC AI"
  },

  "desc":{
    "default": "Survive simulator, using Artificial Intelligence and Genetic Algorithm. Developed with C++.",
    "ptBR": "Simulador de sobrevivência, utilizando Inteligência Artificial e Algoritmo Genético. Desenvolvido com C++.",
    "enEN": "Survive simulator, using Artificial Intelligence and Genetic Algorithm. Developed with C++."
  },

  "thumbnail":{
    "default": img2,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/K_HANjYaGfQ"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/drive/folders/0B1-JF0lRQU09ZW9HNlpXNTRiQUE?usp=sharing",
    "desc": {
      "default": "Download"
    }
  },

  "link": "/tcc_ai",

  tags: ["presentation"],

  "styleContent": {
    "backgroundColor": "rgb(25, 25, 25)",
    "color": "white"
  }
}


