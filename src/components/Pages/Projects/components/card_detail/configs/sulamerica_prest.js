import img from 'images/projects/Sulamerica Prest/logo.jpeg'

import img1 from 'images/projects/Sulamerica Prest/img00.png'
import img2 from 'images/projects/Sulamerica Prest/img01.png'
import img3 from 'images/projects/Sulamerica Prest/img02.png'
import img4 from 'images/projects/Sulamerica Prest/img03.png'

export default {
  "title":{
    "default":"SulAmérica Prestadores",
    "ptBR": "SulAmérica Prestadores",
    "enEN": "SulAmérica"
  },

  "desc":{
    "default": "The PWA made with React.js, together Redux, for internal using of the dentists of SulAmérica.",
    "ptBR": "O PWA feito em React, junto com Redux, para uso interno dos médicos odontologistas da SulAmérica.",
    "enEN": "The PWA made with React.js, together Redux, for internal using of the dentists of SulAmérica."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  // "link_download": {
  //   "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
  //   "desc": {
  //     "default": "Download"
  //   }
  // },
  // "link_promo": {
  //   "url": "http://www.novaquality.com.br/?dealer=nova-quality",
  //   "desc": {
  //     "default": "Conheça o site...",
  //     "ptBR": "Conheça o site...",
  //     // "enEN": "See more details here..."
  //   }
  // }

  // ,

  "link": "/sulamerica_prest",

  tags: ["main", "web"],

  "styleContent": {
    "background": "linear-gradient(#f47521, #ffffff)",
    "color": "#0c2d72"
  }
}
