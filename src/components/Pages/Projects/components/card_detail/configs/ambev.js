import img from 'images/projects/Ambev/ambev.gif'

import img1 from 'images/projects/Ambev/img00.png'
import img2 from 'images/projects/Ambev/img01.png'
import img3 from 'images/projects/Ambev/img02.png'
import img4 from 'images/projects/Ambev/img03.png'
import img5 from 'images/projects/Ambev/img04.png'
import img6 from 'images/projects/Ambev/img05.png'

export default {
  "title":{
    "default":"AmBev Comex",
    "ptBR": "AmBev Comex",
    // "enEN": ""
  },

  "desc":{
    "default": "Internal system to control the coming and departing AmBev products with the foreign trade. Made with React.js and Redux.",
    "ptBR": "Controle interno de entrada e saída dos insumos da AmBev com o comércio exterior (Comex). Feito com React.js e Redux.",
    "enEN": "Internal system to control the coming and departing AmBev products with the foreign trade. Made with React.js and Redux."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  // "link_download": {
  //   "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
  //   "desc": {
  //     "default": "Download"
  //   }
  // },
  // "link_promo": {
  //   "url": "http://www.novaquality.com.br/?dealer=nova-quality",
  //   "desc": {
  //     "default": "Conheça o site...",
  //     "ptBR": "Conheça o site...",
  //     // "enEN": "See more details here..."
  //   }
  // }

  // ,

  "link": "/ambev_comex",

  tags: ["web"],

  "styleContent": {
    "backgroundColor": "white",
    "color": "#0e458d"
  }
}
