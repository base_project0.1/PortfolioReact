import img1 from 'images/projects/Garbage\ Baseball/gb3.png'
import img2 from 'images/projects/Garbage\ Baseball/gb2.png'
import img3 from 'images/projects/Garbage\ Baseball/gb1.png'

export default {
  "title":{
    "default":"Garbage Baseball",
    "ptBR": "",
    "enEN": "Garbage Baseball"
  },

  "desc":{
    "default": "Hit the trash to the correct trash can. The game was developed in the first Week Jam in FATEC of Americana, with the theme garbage, using GameMaker.",
    "ptBR": "Rebata o lixo para sua lixeira correta. Foi desenvolvido na 1a Week Jam da FATEC de Americana, com o tema lixo, usando o GameMaker.",
    "enEN": "Hit the trash to the correct trash can. The game was developed in the first Week Jam in FATEC of Americana, with the theme garbage, using GameMaker."
  },

  "thumbnail":{
    "default": img1,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/OoA-oPIGcMM"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/18KBeLutwxZTT2Tm440AdPBIToXio6e5F/view?usp=sharing",
    "desc": {
      "default": "Download Windows"
    }
  },

  "link": "/garbage_baseball",

  tags: ["other", "game"],

  "styleContent": {
    "color": "white",
    "backgroundColor": "blue"
  }
}
