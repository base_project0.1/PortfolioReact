import img1 from 'images/projects/Math In Games 01/mig01.png'
import img2 from 'images/projects/Math In Games 01/mig02.png'
import img3 from 'images/projects/Math In Games 01/mig03.png'

export default {
  "title":{
    "default":"Math in Games",
    "ptBR": "Matemática nos Games",
    "enEN": "Math in Games"
  },

  "desc":{
    "default": "Presentation in Learning Day on Just Digital company, about the introduction of Math in Games, demystifying the Cartesian Plan, Pythagorean Theorem and Angles.",
    "ptBR": "Apresentação feita no Learning Day da Just Digital, sobre a introdução de Matemática nos Games, desmistificando Plano Cartesiano, Teorema de Pitágoras e Ângulos.",
    "enEN": "Presentation in Learning Day on Just Digital company, about the introduction of Math in Games, demystifying the Cartesian Plan, Pythagorean Theorem and Angles."
  },

  "thumbnail":{
    "default": img1,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  "link_download": {
    "url": "https://gitlab.com/canvasgames/math-in-games-1.git",
    "desc": {
      "ptBR": "Veja o código do projeto...",
      "enEN": "See the code of project..."
    }
  },
  "link_promo": {
    "url": "https://zoom.us/recording/play/lQO_XF1wc_1KwPELC07ghlCjSnbt4yLivPU9uAwNHrokgLMk2215VIGfhWCIppHt?startTime=1556219550000",
    "desc": {
      "ptBR": "Veja a apresentação...",
      "enEN": "See the presentation..."
    }
  },

  "link": "/mathingames01",

  tags: ["presentation"],

  "styleContent": {
    "backgroundColor": "#39314d",
    "color": "white"
  }
}
