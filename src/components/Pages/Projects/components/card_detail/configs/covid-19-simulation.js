import logo from 'images/projects/COVID-19 Simulador/cs_logo.png'

import img1 from 'images/projects/COVID-19 Simulador/cs01.png'
import img2 from 'images/projects/COVID-19 Simulador/cs02.png'
import img3 from 'images/projects/COVID-19 Simulador/cs03.png'
import img4 from 'images/projects/COVID-19 Simulador/cs04.png'

export default {
  "title":{
    "default":"COVID-19 Sim",
    "ptBR": "COVID-19 Simulador",
    "enEN": ""
  },

  "desc":{
    "default": "COVID-19 infection simulator, to show how important is to use mask and 'Stay at home', in this case, stay stopped. Made with <u>React Hooks</u>, <u>API COntext</u> and <u>Canvas</u>.",
    "ptBR": "Simulador da infecção do COVID-19, para mostrar a importância de usar máscara e do movimento 'Fique em casa', que no simulador é ficar parado. Feito com <u>React Hooks</u>, <u>API COntext</u> e <u>Canvas</u>.",
    "enEN": "COVID-19 infection simulator, to show how important is to use mask and 'Stay at home', in this case, stay stopped. Made with <u>React Hooks</u>, <u>API COntext</u> and <u>Canvas</u>."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4,
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/AVqpQuciEog"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://covid-19-simulation.netlify.app/",
    "desc": {
      "ptBR": "Teste o simulador...",
      "enEN": "Try the simulator..."
    }
  },

  "link": "/covid-19_sim",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "rgb(85, 105, 85)",
    "color": "white"
  }
}
