import thumbnail from 'images/projects/Fresco WP/banner-home.png'

import img1 from 'images/projects/Fresco WP/fresco01.png'
import img2 from 'images/projects/Fresco WP/fresco02.png'
import img3 from 'images/projects/Fresco WP/fresco03.png'
import img4 from 'images/projects/Fresco WP/fresco04.png'
import img5 from 'images/projects/Fresco WP/fresco05.png'

export default {
  "title":{
    "default":"Fresco - WordPress",
    // "ptBR": "Abrindo Caminho",
    // "enEN": "Openning Away"
  },

  "thumbnail": {
    "default": thumbnail,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      thumbnail, img1, img2, img3, img4, img5
    ],
    "ptBR": [],
    "enEN": []
  },

  "desc":{
    "default": "Website develop during the course: 'WordPress Course: making a website from zero' of the Youtube channel 'Curso em Video'.",
    "ptBR": "Site feito durante o curso 'Curso de WordPress: criando um site do zero' do canal do Youtube 'Curso em Video'.",
    "enEN": "Website develop during the course: 'WordPress Course: making a website from zero' of the Youtube channel 'Curso em Video'."
  },

  "link_download": {
    "url": "http://fresco.mypressonline.com",
    "desc": {
      "default": "See the website",
      "ptBR": "Veja como ficou...",
      "enEN": "See the website"
    }
  },

  "link": "/fresco-wp",
  tags: ["other"],

  "styleContent": {
    "color": "#3a3a3a",
    "background": "#ff8800"
  }
}
