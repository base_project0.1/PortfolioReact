import img from 'images/projects/Christmas\ Gift/512x512.png'

import img1 from 'images/projects/Christmas\ Gift/Screen Shot 11-08-15 at 02.52 PM.png'
import img2 from 'images/projects/Christmas\ Gift/Screen Shot 11-07-15 at 10.37 PM 001.png'
import img3 from 'images/projects/Christmas\ Gift/Screen Shot 11-11-15 at 02.51 PM.png'

export default {
  "title":{
    "default":"Christmas Gift",
    "ptBR": "Presente de Natal",
    "enEN": "Christmas Gift"
  },

  "desc":{
    "default": "Help the Santa Claus to throw the gifts in the chimneys. The secret is the timing to throw the gifts to hit the target. Enjoy the travel as the Santa Claus and give more gifts that you can.",
    "ptBR": "Ajude o Papai Noel a deixar os presentes nas chaminés. O segredo é ter o timming certo para jogar os presentes e acertar o objetivo. Aproveite o passeio como Papai Noel e distribua a maior quantidade de presentes possíveis.",
    "enEN": "Help the Santa Claus to throw the gifts in the chimneys. The secret is the timing to throw the gifts to hit the target. Enjoy the travel as the Santa Claus and give more gifts that you can."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath":null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/1_W9gdRoQMx5Sj-c7BjrlKGl09xd7lYGY/view?usp=sharing",
    "desc": {
      "default": "Download"
    }
  },

  "link": "/christmas_gift",

  tags: ["game"],

  "styleContent": {
    "background": "linear-gradient(to right, rgb(255, 63, 63), rgb(144, 144, 0), rgb(0, 218, 0), rgba(144, 144, 0, 1), rgb(255, 63, 63))",
    "color": "white"
  }
}
