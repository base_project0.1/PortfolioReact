import img1 from 'images/projects/CET2018/screenshot_3.png'
import img2 from 'images/projects/CET2018/screenshot_6.png'
import img3 from 'images/projects/CET2018/screenshot_9.png'
import img4 from 'images/projects/CET2018/screenshot_12.png'
import img5 from 'images/projects/CET2018/screenshot_13.png'
import img6 from 'images/projects/CET2018/screenshot_15.png'
import img7 from 'images/projects/CET2018/screenshot_14.png'

export default {
  "title":{
    "default":"CET 2018",
  },

  "desc":{
    "default": "Game made for CET 2018 Educative Games Competition. The objective is to do the player identify the mistakes through the Transit Laws.",
    "ptBR": "Jogo feito para o Concurso de Jogos Educativos da CET de 2018, onde o objetivo é fazer com que o jogador identifique os erros perante as Leis de Trânsito.",
    "enEN": "Game made for CET 2018 Educative Games Competition. The objective is to do the player identify the mistakes through the Transit Laws."
  },

  "thumbnail":{
    "default": img1,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6, img7
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/uXk_y6LlsFM"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://giovanniportfolio.000webhostapp.com/",
    "desc": {
      "ptBR": "Jogue agora!",
      "enEN": "Play now!"
    }
  },

  "link": "/cet2018",

  tags: ["game"],

  "styleContent": {
    "backgroundColor": "#0c0523",
    "color": "white"
  }
}
