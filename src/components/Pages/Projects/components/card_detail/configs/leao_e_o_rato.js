import thumbnail from 'images/projects/Leao e o Rato/logo.png'

import img1 from 'images/projects/Leao e o Rato/dc01.png'
import img2 from 'images/projects/Leao e o Rato/dc02.png'
import img3 from 'images/projects/Leao e o Rato/dc03.png'
import img4 from 'images/projects/Leao e o Rato/dc04.png'
import img5 from 'images/projects/Leao e o Rato/dc05.png'

export default {
  "title":{
    "default":"Leão e o Ratinho",
    "ptBR": "Leão e o Ratinho",
    "enEN": "The lion and the mouse"
  },

  "thumbnail": {
    "default": thumbnail,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img2, img3, img4, img5, img1,
    ],
    "ptBR": [],
    "enEN": []
  },

  "desc":{
    "default": "Plataform of creation of <u>SCORM</u> courses, using <u>React.js</u> and development by me. To show all features develop until now, I use the fable 'The lion and the mouse'.",
    "ptBR": "Plataforma de criação de cursos <u>SCORM</u>, utilizando <u>React.js</u> e desenvolvida por mim. Para mostrar as funcionalidades implementadas, utilizei a Fábula do Leão e do Ratinho.",
    "enEN": "Plataform of creation of <u>SCORM</u> courses, using <u>React.js</u> and development by me. To show all features develop until now, I use the fable 'The lion and the mouse'."
  },

  "link_download": {
    "url": "https://course-demo.netlify.app/",
    "desc": {
      "default": "See the website",
      "ptBR": "Veja como ficou...",
      "enEN": "See the website"
    }
  },

  "link": "/demo-course",
  tags: ["web"],

  "styleContent": {
    "color": "#000",
    "background": "linear-gradient(to top, #e3d1b0, #f0f0f5, #e3d1b0)"
  }
}
