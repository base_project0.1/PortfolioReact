import logo from 'images/projects/Pandemusic/logo.png'

import img1 from 'images/projects/Pandemusic/pm01.png'
import img2 from 'images/projects/Pandemusic/pm02.png'
import img3 from 'images/projects/Pandemusic/pm03.png'
import img4 from 'images/projects/Pandemusic/pm04.png'

export default {
  "title":{
    "default":"Pandemusic",
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "My first project made with <u>TypeScript</u>, using <u>Next.js</u> and <u>Mobile First</u> style method way as well.",
    "ptBR": "Meu 1º projeto feito com <u>TypeScript</u>, utilizando <u>Next.js</u> e o método de estilo <u>Mobile First</u>.",
    "enEN": "My first project made with <u>TypeScript</u>, using <u>Next.js</u> and <u>Mobile First</u> style method way as well."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://pandemusic.vercel.app/",
    "desc": {
      "ptBR": "Veja o site...",
      "enEN": "See the website..."
    }
  },

  "link": "/pandemusic",

  tags: ["other"],

  "styleContent": {
    "backgroundColor": "#5c7689",
    "color": "white"
  }
}
