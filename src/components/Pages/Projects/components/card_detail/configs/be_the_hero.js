import thumbnail from 'images/projects/Be The Hero/logo.png'

import img1 from 'images/projects/Be The Hero/home.png'
import img2 from 'images/projects/Be The Hero/new-incident.png'
import img3 from 'images/projects/Be The Hero/profile.png'
import img4 from 'images/projects/Be The Hero/register.png'

export default {
  "title":{
    "default":"Be The Hero",
    // "ptBR": "Abrindo Caminho",
    // "enEN": "Openning Away"
  },

  "thumbnail": {
    "default": thumbnail,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4,
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/oI0_RGjUY1M"
    ],
    "ptBR": [],
    "enEN": []
  },

  "desc":{
    "default": "App frontend and backend develop in a course of OmniStack Week 11.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning React Hooks, Expo and how to do a deploy a backend in Heroku plataform.",
    "ptBR": "Feito para a semana Semana OmniStack 11.0 da Rockseat, utilizando <u>React.js</u>, <u>React Native</u> e <u>Node.js</u>. Além de aprender a utilizar o React Hooks, o Expo para desenvolver mobile e deploy do backend no Heroku.",
    "enEN": "App frontend and backend develop in a course of OmniStack Week 11.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning React Hooks, Expo and how to do a deploy a backend in Heroku plataform."
  },

  "link_download": {
    "url": "https://gdev-be-the-hero.netlify.app/",
    "desc": {
      "default": "See the website",
      "ptBR": "Veja como ficou...",
      "enEN": "See the website"
    }
  },

  "link": "/be-the-hero",
  tags: ["other"],

  "styleContent": {
    "color": "#41414d",
    "background": "linear-gradient(to top, #f0f0f5, #e02041, #f0f0f5)"
  }
}
