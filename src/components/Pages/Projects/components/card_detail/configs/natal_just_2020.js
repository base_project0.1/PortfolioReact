import img from 'images/projects/Natal Just 2020/screenshot.png'

import img1 from 'images/projects/Natal Just 2020/screenshot_55.png'
import img2 from 'images/projects/Natal Just 2020/screenshot_7.png'

export default {
  "title":{
    "default":"Natal Just Digital 2020",
    "ptBR": "Natal Just Digital 2020",
    "enEN": "Just Digital Christmas 2020"
  },

  "desc":{
    "default": "Landing page made with React.js, to the Children Christmas campaign of Santo Amaro, São Paulo.",
    "ptBR": "Landing page feita em React.js, para a campanha de Natal das crianças da 'Casa da Criança e do Adolescente de Santo Amaro'.",
    "enEN": "Landing page made with React.js, to the Children Christmas campaign of Santo Amaro, São Paulo."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://natal-just-2019-homolog.netlify.com/",
    "desc": {
      "ptBR": "Veja o link de homologação!",
      "enEN": "See the homologation link!"
    }
  },

  "link": "/natal_just_2020",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "#5f528d",
    "color": "white"
  }
}
