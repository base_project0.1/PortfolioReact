import logo from 'images/projects/GDev/gdev_logo.png'

import img1 from 'images/projects/GDev/gdev01.png'
import img2 from 'images/projects/GDev/gdev02.png'
import img3 from 'images/projects/GDev/gdev03.png'

export default {
  "title":{
    "default":"GDev",
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "GDev Web Site Develp company Website, using <u>WorldPress</u> CMS.",
    "ptBR": "Site feito para a empresa GDev de desenvolvimento web, utilizando o CMS <u>WorldPress</u>.",
    "enEN": "GDev Web Site Develp company Website, using <u>WorldPress</u> CMS."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://gdev-web.000webhostapp.com/",
    "desc": {
      "ptBR": "Veja o site...",
      "enEN": "See the website..."
    }
  },

  "link": "/gdev",

  tags: ["web"],

  "styleContent": {
    "backgroundColor": "#0c570a",
    "color": "#eeeeee"
  }
}
