import thumbnail from 'images/projects/Ecoleta/logo_icon.png'

import img1 from 'images/projects/Ecoleta/eco01.png'
import img2 from 'images/projects/Ecoleta/eco02.png'
import img3 from 'images/projects/Ecoleta/eco03.png'

export default {
  "title":{
    "default":"Ecoleta",
    // "ptBR": "Abrindo Caminho",
    // "enEN": "Openning Away"
  },

  "thumbnail": {
    "default": thumbnail,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "desc":{
    "default": "App frontend and backend develop in a course of Next Level Week of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks and Expo.",
    "ptBR": "Feito para a semana Next Level Week da Rockseat, utilizando <u>React.js</u>, <u>React Native</u> e <u>Node.js</u>. Além de aprender a utilizar o TypeScript, React Hooks e o Expo.",
    "enEN": "App frontend and backend develop in a course of Next Level Week of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks and Expo."
  },

  // "link_download": {
  //   "url": "https://gdev-be-the-hero.netlify.app/",
  //   "desc": {
  //     "default": "See the website",
  //     "ptBR": "Veja como ficou...",
  //     "enEN": "See the website"
  //   }
  // },

  "link": "/ecoleta",
  tags: ["other"],

  "styleContent": {
    "color": "#f0eff4",
    "background": "#3cc96d"
  }
}
