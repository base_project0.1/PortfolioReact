import img from 'images/projects/Toyota DDS/logo.png'

import img1 from 'images/projects/Toyota DDS/img00.png'
import img2 from 'images/projects/Toyota DDS/img01.png'
import img3 from 'images/projects/Toyota DDS/img02.png'
import img4 from 'images/projects/Toyota DDS/img03.png'

export default {
  "title":{
    "default":"Toyota DDS",
    "ptBR": "Toyota DDS",
    // "enEN": ""
  },

  "desc":{
    "default": "Developed for the Dealers of Toyota, for them have a easy and intuitive form to create your sales sities. The project was made with Drupal CMS, using Javascript, jQuery, Twig.js, PHP, Docker and Git.",
    "ptBR": "Feito para os Dealers da Toyota terem um opção fácil e intuitiva para criar seu próprio site de vendas. Projeto desenvolvido com o CMS Drupal, utilizando Javascript, jQuery, Twig.js, PHP, Docker e Git.",
    "enEN": "Developed for the Dealers of Toyota, for them have a easy and intuitive form to create your sales sities. The project was made with Drupal CMS, using Javascript, jQuery, Twig.js, PHP, Docker and Git."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  // "link_download": {
  //   "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
  //   "desc": {
  //     "default": "Download"
  //   }
  // },
  "link_promo": {
    "url": "http://www.novaquality.com.br/?dealer=nova-quality",
    "desc": {
      "default": "Conheça o site...",
      "ptBR": "Conheça o site...",
      // "enEN": "See more details here..."
    }
  },

  "link": "/toyota_dds",

  tags: ["main", "web"],

  "styleContent": {
    "background": "linear-gradient(#ffffff, #ededed)",
    "color": "#2b2b2b"
  }
}
