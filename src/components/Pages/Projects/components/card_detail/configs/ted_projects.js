import img from 'images/projects/TED/ted.png'

import img1 from 'images/projects/TED/img00.png'
import img2 from 'images/projects/TED/img01.png'
import img3 from 'images/projects/TED/img02.png'
import img4 from 'images/projects/TED/img03.png'
import img5 from 'images/projects/TED/img04.png'
import img6 from 'images/projects/TED/img05.png'
import img7 from 'images/projects/TED/img06.png'

export default {
  "title":{
    "default":"Telefónica Educação Digital",
    "ptBR": "Telefónica Educação Digital",
    // "enEN": "Wizards Potion"
  },

  "desc":{
    "default": "E-learnings made with Pixi.js, React.js and Unity 3D (C#). The main customers was: Porto Seguro, OdontoPrev, Vivo, etc.",
    "ptBR": "Diversos e-learnings feitos com Pixi.js, React.js e Unity 3D (C#), tendo como principais clientes Porto Seguro, OdontoPrev, Vivo, etc.",
    "enEN": "E-learnings made with Pixi.js, React.js and Unity 3D (C#). The main customers was: Porto Seguro, OdontoPrev, Vivo, etc."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6, img7
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  // "link_download": {
  //   "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
  //   "desc": {
  //     "default": "Download"
  //   }
  // },
  // "link_promo": {
  //   "url": "http://www.novaquality.com.br/?dealer=nova-quality",
  //   "desc": {
  //     "default": "Conheça o site...",
  //     "ptBR": "Conheça o site...",
  //     // "enEN": "See more details here..."
  //   }
  // }

  // ,

  "link": "/ted",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "#07394a",
    "color": "white"
  }
}
