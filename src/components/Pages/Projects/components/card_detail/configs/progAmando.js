import img1 from 'images/projects/progAmando/pa_01.png'
import img2 from 'images/projects/progAmando/pa_02.png'
import img3 from 'images/projects/progAmando/pa_03.png'
import img4 from 'images/projects/progAmando/pa_04.png'
import img5 from 'images/projects/progAmando/pa_05.png'

import logo from 'images/projects/progAmando/logo.png'

export default {
  "title":{
    "default":"progAmando",
    "ptBR": "",
    "enEN": "progAmando"
  },

  "desc":{
    "default": "Project developed with <u>Next.js</u> and <u>Contentful CMS</u>. It's my blog about development world, besides share my learnings to the comunite.",
    "ptBR": "Projeto feito com <u>Next.js</u> e <u>CMS Contetful</u>. Meu blog sobre assuntos de programação e compartilhamento de meus aprendizados com a comunidade.",
    "enEN": "Project developed with <u>Next.js</u> and <u>Contentful CMS</u>. It's my blog about development world, besides share my learnings to the comunite."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://gitlab.com/next.js-projects/prog_amando",
    "desc": {
      "ptBR": "Código...",
      "enEN": "Code..."
    }
  },
  "link_promo": {
    "url": "https://prog-amando.vercel.app/",
    "desc": {
      "ptBR": "Conheça meu blog...",
      "enEN": "Read my blog..."
    }
  },

  "link": "/progAmando",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "#282a2f",
    "color": "#68caf0"
  }
}
