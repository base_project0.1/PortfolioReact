import img1 from 'images/projects/Squeezing\ Acne/sta_promo.png'
import img2 from 'images/projects/Squeezing\ Acne/sta_screen_shoot1_thumb.png'
import img3 from 'images/projects/Squeezing\ Acne/sta_screen_shoot5.png'
import img4 from 'images/projects/Squeezing\ Acne/sta_screen_shoot7.png'
import img5 from 'images/projects/Squeezing\ Acne/sta_screen_shoot1.png'

export default {
  "title":{
    "default":"Squeezing the Acne",
    "ptBR": "Espremendo a Acne",
    "enEN": "Squeezing the Acne"
  },

  "desc":{
    "default": "Have the feeling to squeezing some acnes on the back of a person.\nThis game is a test of mult touch of Android Devices.",
    "ptBR": "Tenha a sensação de espremer espinha das costas de uma pessoa.\nFeito para testar o multitouch dos aparelhos Android.",
    "enEN": "Have the feeling to squeezing some acnes on the back of a person.\nThis game is a test of mult touch of Android Devices."
  },

  "thumbnail":{
    "default": img2,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/1dd5zZGqHpO3CJ-uOs89sJRmwZUtJu0eh/view?usp=sharing",
    "desc": {
      "default": "Download"
    }
  },

  "link": "/squeezing_acne",

  tags: ["other", "game"],

  "styleContent": {
    "color": "white",
    "background": "linear-gradient(to right, rgb(255, 186, 186), rgb(175, 42, 42), rgb(241, 167, 167))"
  },
  "styleDesc":{
    "backgroundColor": "#5000003d"
  }
}
