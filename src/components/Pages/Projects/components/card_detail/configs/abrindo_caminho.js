import thumbnail from 'images/projects/Abrindo Caminho/ac_start.png'

import img1 from 'images/projects/Abrindo\ Caminho/ac_level1.png'
import img2 from 'images/projects/Abrindo\ Caminho/ac_drwilly.png'
import img3 from 'images/projects/Abrindo\ Caminho/ac_bowser.png'
import img4 from 'images/projects/Abrindo\ Caminho/ac_bison2.png'
import img5 from 'images/projects/Abrindo\ Caminho/ac_bison.png'
import img6 from 'images/projects/Abrindo\ Caminho/ac_control.png'

export default {
  "title":{
    "default":"Openning Away",
    "ptBR": "Abrindo Caminho",
    "enEN": "Openning Away"
  },

  "thumbnail": {
    "default": thumbnail,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/ouMbf1eDvbE"
    ],
    "ptBR": [],
    "enEN": []
  },

  "desc":{
    "default": "Intergalactic race, where the goal is detroy the space ships to win the race. The differential of this shooter is that you can aim the crosshair with right analog to direct a bullet.",
    "ptBR": "Corrida intergalática, onde o objetivo é destruir as naves rivais para chegar em primeiro lugar. O diferencial desse shooter é poder mirar com o analógico direito a direção do tiro.",
    "enEN": "Intergalactic race, where the goal is detroy the space ships to win the race. The differential of this shooter is that you can aim the crosshair with right analog to direct a bullet."
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/1_W9gdRoQMx5Sj-c7BjrlKGl09xd7lYGY/view?usp=sharing",
    "desc": {
      "default": "Download Windows"
    }
  },

  "link": "/openning_away",
  tags: ["game"],

  "styleContent": {
    "color": "white",
    "background": "linear-gradient(to right, #00005f, #000000, #00005f)"
  }
}
