import logo from 'images/projects/Chefe\ Gio/cg_logo.png'

import img1 from 'images/projects/Chefe\ Gio/cg01.png'
import img2 from 'images/projects/Chefe\ Gio/cg02.png'
import img3 from 'images/projects/Chefe\ Gio/cg03.png'

export default {
  "title":{
    "default":"Chefe Gio",
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "Homemade recipes website, develop in <u>React Hooks</u>, using <u>mobile first style</u> way.",
    "ptBR": "Site de receitas caseiras feito em <u>React Hooks</u>, utilizando a estilização <u>mobile first</u>",
    "enEN": "Homemade recipes website, develop in <u>React Hooks</u>, using <u>mobile first style</u> way."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://chefe-gio.vercel.app/",
    "desc": {
      "ptBR": "Veja o site...",
      "enEN": "See the website..."
    }
  },

  "link": "/chefe_gio",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "rgb(166, 24, 39)",
    "color": "white"
  }
}
