import img1 from 'images/projects/Math\ Athlete/ma_0.webp'
import img2 from 'images/projects/Math\ Athlete/ma_1.webp'
import img3 from 'images/projects/Math\ Athlete/ma_2.webp'
import img4 from 'images/projects/Math\ Athlete/ma_3.webp'
import img5 from 'images/projects/Math\ Athlete/ma_4.webp'
import img6 from 'images/projects/Math\ Athlete/icon.png'

export default {
  "title":{
    "default":"Math Athlete",
    "ptBR": "Matléta",
    "enEN": "Math Athlete"
  },

  "desc":{
    "default": "The athlete run by math, find out the 'x' value and don\'t let him stop. Solve the problems ASAP and keep your atention in the track, to cacth the cup of water and the coins.\nThe game has 10 characters that you could unlocked.",
    "ptBR": "Movido pela matemática, descubra o valor do 'x' e não deixe o atléta parar.\nFaça os cálculos o mais rápido possível e mantenha sua atenção na pista para não perder a água e as moedas\n O jogo contém 10 personagens desbloqueaveis.",
    "enEN": "The athlete run by math, find out the 'x' value and don\'t let him stop. Solve the problems ASAP and keep your atention in the track, to cacth the cup of water and the coins.\nThe game has 10 characters that you could unlocked."
  },

  "thumbnail":{
    "default": img6,
    // "ptBR": "",
    // "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/11lCv5gr_eYiipUUmuTxdCzENVQ3vpQtm/view?usp=sharing",
    "desc": {
      "default": "Download"
    }
  },

  "link": "/math_athlete",

  tags: ["game"],

  "styleContent": {
    "backgroundColor": "#ff8a8a",
    "color": "black"
  }
}
