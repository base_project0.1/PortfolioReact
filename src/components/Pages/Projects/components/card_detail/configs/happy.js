import img1 from 'images/projects/Happy/nlw3_01.png'
import img2 from 'images/projects/Happy/nlw3_02.png'
import img3 from 'images/projects/Happy/nlw3_03.png'
import img4 from 'images/projects/Happy/nlw3_04.png'

import logo from 'images/projects/Happy/logo.png'

export default {
  "title":{
    "default":"Happy",
  },

  "desc":{
    "default": "App frontend and backend develop in a course of Next Level Week 2.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks, Expo, Leaflet and TypeOrm.",
    "ptBR": "Feito para a semana Next Level Week 3.0 da Rockseat, utilizando <u>React.js</u>, <u>React Native</u> e <u>Node.js</u>. Além de aprender a utilizar o TypeScript, React Hooks, o Expo, Leaflet e TypeOrm.",
    "enEN": "App frontend and backend develop in a course of Next Level Week 2.0 of Rockseat, using <u>React.js</u>, <u>React Native</u> and <u>Node.js</u>. Besides learning TypeScript, React Hooks, Expo, Leaflet and TypeOrm."
  },

  "thumbnail":{
    "default": logo,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4,
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://gitlab.com/nextlevelweek03",
    "desc": {
      "default": "Code...",
      "ptBR": "Código...",
      "enEN": "Code...",
    }
  },

  "link": "/happy",

  tags: ["other"],

  "styleContent": {
    "backgroundColor": "#00b2cd",
    "color": "#f7f7f7"
  }
}
