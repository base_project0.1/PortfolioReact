import img1 from 'images/projects/Shock\ Sheep/ss_game_day2.png'
import img2 from 'images/projects/Shock\ Sheep/ss_game_night2.png'
import img3 from 'images/projects/Shock\ Sheep/ss_main_menu_bg.png'

export default {
  "title":{
    "default":"Shock Sheep",
    "ptBR": "",
    "enEN": "Shock Sheep"
  },

  "desc":{
    "default": "Test your reflex, hit only the sheeps of same color of the thunder. Play in Android devices, with Ranking and achievements. Challenge your friends!",
    "ptBR": "Feito para testar seus reflexos, acerte apenas as ovelhas da mesma cor que o raio. Disponível para aparelhos Android, com Ranking e recompensas. Desafie seus amigos!",
    "enEN": "Test your reflex, hit only the sheeps of same color of the thunder. Play in Android devices, with Ranking and achievements. Challenge your friends!"
  },

  "thumbnail":{
    "default": img3,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/g6YqgBscwx0"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://drive.google.com/file/d/1nCLACX1ZxMBzPROXb2PnunUPZzf40AMV/view?usp=sharing",
    "desc": {
      "default": "Download Android"
    }
  },

  "link_promo": {
    "url": "https://drive.google.com/file/d/1FgHxqwThpmcPiniDVLGLFp5L5D4AbE-M/view?usp=sharing",
    "desc": {
      "default": "Download Windows"
    }
  },

  "link": "/shock_sheep",

  tags: ["main", "game"],

  "styleContent": {
    "backgroundColor": "white",
    "color": "black"
  }
}
