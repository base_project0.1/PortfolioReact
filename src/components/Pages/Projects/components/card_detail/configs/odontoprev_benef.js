import img from 'images/projects/OdontoPrev Benef/logo.jpeg'

import img1 from 'images/projects/OdontoPrev Benef/img00.png'
import img2 from 'images/projects/OdontoPrev Benef/img01.png'
import img3 from 'images/projects/OdontoPrev Benef/img02.png'

export default {
  "title":{
    "default":"OdontoPrev Beneficiários",
    "ptBR": "OdontoPrev Beneficiários",
    "enEN": "OdontoPrev Beneficiaries"
  },

  "desc":{
    "default": "Made to help the beneficiary to understand the resources of your own dental plan, for decrease the calls to call center. Project developed with the Drupal CMS, using Vue.js, Twig.js, PHP, Docker, Git and Gulp.",
    "ptBR": "Feito para facilitar o entendimento dos recursos que o próprio beneficiário tem de seu plano odontológico. Projeto desenvolvido com o CMS Drupal, utilizando Vue.js, Twig.js, PHP, Docker, Git and Gulp.",
    "enEN": "Made to help the beneficiary to understand the resources of your own dental plan, for decrease the calls to call center. Project developed with the Drupal CMS, using Vue.js, Twig.js, PHP, Docker, Git and Gulp."
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  // "videos": {
  //   "defaultPath": "",
  //   "defaultVideos": [
  //     "https://youtu.be/125d-jTTd3k"
  //   ],
  //   "ptBR": [],
  //   "enEN": []
  // },

  // "link_download": {
  //   "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
  //   "desc": {
  //     "default": "Download"
  //   }
  // },
  "link_promo": {
    "url": "https://beneficiario.odontoprev.com.br/",
    "desc": {
      "default": "Conheça o site...",
      "ptBR": "Conheça o site...",
      // "enEN": "See more details here..."
    }
  },

  "link": "/odontoprev_benef",

  tags: ["main", "web"],

  "styleContent": {
    "background": "linear-gradient(#0790d1, #114998)",
    "color": "white"
  }
}
