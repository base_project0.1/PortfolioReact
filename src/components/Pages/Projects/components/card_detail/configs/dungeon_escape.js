import img1 from 'images/projects/Dungeon Escape/Capturar.png'
import img2 from 'images/projects/Dungeon Escape/Capturar2.png'
import img3 from 'images/projects/Dungeon Escape/Capturar3.png'
import img4 from 'images/projects/Dungeon Escape/Capturar4.png'
import img5 from 'images/projects/Dungeon Escape/Capturar5.png'
import img6 from 'images/projects/Dungeon Escape/icon.webp'

export default {
  "title":{
    "default":"Dungeon Escape",
    "ptBR": null,
    "enEN": null
  },

  "desc":{
    "default": "Platform game, made through the \'The Ultimate Guide to 2D Mobile Game Development with Unity\' course.",
    "ptBR": "Jogo de plataforma, feito através do curso: \'The Ultimate Guide to 2D Mobile Game Development with Unity\'.",
    "enEN": "Platform game, made through the \'The Ultimate Guide to 2D Mobile Game Development with Unity\' course."
  },

  "thumbnail":{
    "default": img6,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/KnpshVf58NE"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://play.google.com/store/apps/details?id=com.GioDev.DungeonEscape",
    "desc": {
      "ptBR": "Baixe o pela Google Play...",
      "enEN": "Download by Google Play..."
    }
  },

  "link": "/dungeon_escape",

  tags: ["game"],

  "styleContent": {
    "backgroundColor": "rgb(95, 0, 0)",
    "color": "white"
  }
}


