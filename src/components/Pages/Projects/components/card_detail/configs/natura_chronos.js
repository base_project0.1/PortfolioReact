import img from 'images/projects/Natura Chronos/screenshot_1.png'

import img1 from 'images/projects/Natura Chronos/screenshot.png'
import img2 from 'images/projects/Natura Chronos/screenshot_2.png'
import img3 from 'images/projects/Natura Chronos/screenshot_3.png'
import img4 from 'images/projects/Natura Chronos/screenshot_4.png'
import img5 from 'images/projects/Natura Chronos/screenshot_5.png'
import img6 from 'images/projects/Natura Chronos/screenshot_6.png'

export default {
  "title":{
    "default":"Natura Chronos",
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "E-learning project using React.js together with SCORM plataform, to teach the employees of Natura for the new products about Chronos line. I working with the collegues of Film3 (https://film3.com.br/)",
    "ptBR": "Projeto de e-learning utilizando React.js junto com SCORM, feito para treinar funcionários da Natura para os novos produtos da linha Chronos. Trabalhando junto com funcionários da Film3 (https://film3.com.br/)",
    "enEN": "E-learning project using React.js together with SCORM plataform, to teach the employees of Natura for the new products about Chronos line. I working with the collegues of Film3 (https://film3.com.br/)"
  },

  "thumbnail":{
    "default": img,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4, img5, img6
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_promo": {
    "url": "https://chronos-homolog.netlify.com/",
    "desc": {
      "ptBR": "Veja o link de homologação!",
      "enEN": "See the homologation link!"
    }
  },

  "link": "/natura_chronos",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "#cdad96",
    "color": "white"
  }
}
