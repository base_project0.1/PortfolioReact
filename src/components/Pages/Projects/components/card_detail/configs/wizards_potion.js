import img1 from 'images/projects/Wizards\ Potion/ScreenShotII.png'
import img2 from 'images/projects/Wizards\ Potion/ScreenShotII2.png'
import img3 from 'images/projects/Wizards\ Potion/ScreenShotIII.png'

export default {
  "title":{
    "default":"Wizards Potion",
    "ptBR": "",
    "enEN": "Wizards Potion"
  },

  "desc":{
    "default": "Game made in 2 days for Global Game Jam at year of 2016, with the ritual theme.\nIn Wizards Potion the goal is push the monster on the cauldron, according to the recipe, to make a potion.",
    "ptBR": "Jogo feito em apenas 2 dias para a Global Game Jam do ano de 2016, com o tema ritual.\nEm Wizards Potion o objetivo é empurrar os monstros no caldeirão, de acordo com o que é pedido na receita, para produzir as poções.",
    "enEN": "Game made in 2 days for Global Game Jam at year of 2016, with the ritual theme.\nIn Wizards Potion the goal is push the monster on the cauldron, according to the recipe, to make a potion."
  },

  "thumbnail":{
    "default": img2,
    "ptBR": "",
    "enEN": ""
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3,
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/125d-jTTd3k"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link_download": {
    "url": "https://ggj.s3.amazonaws.com/games/2016/01/31/1457/Wizard%27s%20Potion.zip",
    "desc": {
      "default": "Download"
    }
  },
  "link_promo": {
    "url": "https://globalgamejam.org/2016/games/wizards-potion",
    "desc": {
      "ptBR": "Veja mais detalhes...",
      "enEN": "See more details here..."
    }
  },

  "link": "/wizards_potion",

  tags: ["main","game"],

  "styleContent": {
    "backgroundColor": "#650967",
    "color": "white"
  }
}
