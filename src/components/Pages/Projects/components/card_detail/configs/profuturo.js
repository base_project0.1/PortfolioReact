import img1 from 'images/projects/ProFuturo/Screen Shot 02-27-17 at 12.23 PM.png'
import img2 from 'images/projects/ProFuturo/Screen Shot 02-27-17 at 12.24 PM.png'
import img3 from 'images/projects/ProFuturo/Screen Shot 02-27-17 at 12.26 PM.png'
import img4 from 'images/projects/ProFuturo/screenshot.png'

export default {
  "title":{
    "default":"Pró Futuro",
    "ptBR": null,
    "enEN": null
  },

  "thumbnail":{
    "default": img4,
    "ptBR": "",
    "enEN": ""
  },

  "desc":{
    "default": "E-learnings developed to Angola\'s children, for them learning in a playful way! Made for tablet devices.",
    "ptBR": "E-learnings desenvolvidos para o aprendizado das crianças da Angola, utilizando tablets.",
    "enEN": "E-learnings developed to Angola\'s children, for them learning in a playful way! Made for tablet devices."
  },

  "images": {
    "defaultPath": null,
    "defaultImgs": [
      img1, img2, img3, img4
    ],
    "ptBR": [],
    "enEN": []
  },

  "videos": {
    "defaultPath": "",
    "defaultVideos": [
      "https://youtu.be/YN8NU3ZOY6k"
    ],
    "ptBR": [],
    "enEN": []
  },

  "link": "/profuturo",

  tags: ["main", "web"],

  "styleContent": {
    "backgroundColor": "#e4b077",
    "color": "white"
  }
}

