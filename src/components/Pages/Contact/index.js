import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as LanguageActions} from 'reducers/Language'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'
import LggeFct, {getValidLanguageTxt, getValidArrayImgs} from 'API_Utils/languageVerify.js'

import TweenMax from 'gsap'

// import Pagination from 'components/Pagination'
//
// import Loading from 'components/Loading'

// import 'fontAwesome'

import { Link } from "react-router-dom";

//Config
import translateConfig from "./config/translateConfig.json"
import linksConfig from "pages/Fixed/config/linksConfig.js"

class Contact extends Component {

  constructor(props) {
    super(props)

    this.toggleTextOfIcon = this.toggleTextOfIcon.bind(this)
  }

  componentDidMount() {

    const linksText = document.querySelectorAll('.contact-page .mainLinks>div:not(.inline-links)')
    const linksAnother = document.querySelectorAll('.contact-page .mainLinks .inline-links div')

    TweenMax.to([...linksText, ...linksAnother], 0, {opacity: 0})

    TweenMax.fromTo('.contact-page form', 1, 
      {opacity: 0},
      {opacity: 1,
      onComplete: () => {
        TweenMax.fromTo(linksText[0], .5, 
          {opacity: 0, left: '-=200', position: 'relative'},
          {opacity: 1, left: '+=200', position: 'relative'}
        )
        TweenMax.fromTo(linksText[1], .5, 
          {opacity: 0, right: '-=200', position: 'relative'},
          {opacity: 1, right: '+=200', position: 'relative'}
        )
        
        TweenMax.staggerFromTo('.contact-page .mainLinks .inline-links div', .2, 
        {opacity: 0, bottom: '-=200', position: 'relative'}, 
        {opacity: 1, bottom: '+=200', position: 'relative'}, 
        .25)

      }}
    )

  }

  createMainLink(config) {
    return (
      <div className="mainLinks">
        {config.slice(0,2).map( (item, id) => this.createLink(item, id))}
        <div className="inline-links">{config.slice(2).map( (item, id) => this.createLink(item, id))}</div>
      </div>
    )
  }

  createLink(item, id){
    const iconConfig = {
      title: item.title,
      className: "fa fa-".concat(item.icon),
      onClick: item.text && (() => { this.toggleTextOfIcon("inputMainLinks"+id) }),
    }

    const icon = <i {...iconConfig} />
    const component = item.link ?
      <a href={item.link} target="_blank">{icon}</a>
      :
      <div>
        {icon}
        <div className="linkDesc">
          {item.text}
        </div>
      </div>
    return <div key={id}>{component}</div>
  }

  toggleTextOfIcon(name) {
    const allLinksDesc = this.linksDesc
    const cInput = allLinksDesc[name]
    if(cInput.style.display === 'none'){
      Object.keys(this.linksDesc).forEach(name => this.linksDesc[name].style.display = 'none')
      cInput.style.display = 'initial'
    }
    else{
      cInput.style.display = 'none'
    }
  }

  sendEmail({name, email, subject, message}) {

    const {current} = this.props

    const { success_send_mail_msg, fail_send_mail_msg } = translateConfig

    emailjs.init("user_9qh5qtJIkCqj2upVU0aEJ")
    emailjs.send(
      "gmail", 
      "giovanni_portfolio", 
      {
        "name":name.value,
        "email":email.value,
        "subject":subject.value,
        "body": message.value
      })

    const msg = getValidLanguageTxt(success_send_mail_msg, current)
    alert(msg)

    return false
  }

  render() {

    const {current} = this.props

    const {
      name, your_email, subject, message, send_btn,
      optional_field, required_field,
      success_send_mail_msg, fail_send_mail_msg
    } = translateConfig

    return (
      <Style>
        <div className="contact-page" style={{color:'white'}}>
          <form action={'/'}
            onSubmit={() => this.sendEmail({
                name: this.name,
                email: this.email,
                subject: this.subject,
                message: this.message,
            })}
          >

            <Grid fluid>
              <Row>
                <Col xs={12} sm={6} md={4} lg={4}>
                  <label className="captalize-txt">{getValidLanguageTxt(name, current)}:</label>
                  <br />
                  <input type="text" name="name" id="name" ref={r=> this.name = r} required />
                  <br />

                  <label className="captalize-txt">{getValidLanguageTxt(your_email, current)}:</label>
                  <br />
                  <input type="text" name="email" id="email" ref={r=>this.email = r} required />
                  <br />

                  <label className="captalize-txt">{getValidLanguageTxt(subject, current)}:</label>
                  <br />
                  <input type="text" name="subject" id="subject" ref={r=>this.subject = r} required />
                </Col>
                <Col xs={12} sm={6} md={8} lg={8}>
                  <label className="captalize-txt">{getValidLanguageTxt(message, current)}:</label>
                  <br />
                  <textarea rows="4" name="message" id="message" ref={r=>this.message = r} required />
                </Col>
              </Row>
            </Grid>

            <input className="captalize-txt" type="submit" value={getValidLanguageTxt(send_btn, current)}/>
          </form>

          {/* {this.createMainLink(linksConfig)} */}

        </div>
      </Style>
    )
  }

}

function mapStateToProps( {languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...LanguageActions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact)
