import styled from 'styled-components'

const style = styled.div`

  height: 100%;
  width: 100%;

  padding-top: 25px !important;

  .contact-page {

    .squad-disabled {
      opacity: 0.3;
      user-select: none;
      pointer-events: unset;
      cursor: auto !important;
    }

    padding: 30px;
    margin: 10px;

    .mainLinks {

      padding-top: 20px;
      text-align: center;

      .inline-links {
        margin-top: 10px;
        display: inline-flex;

        margin-right: -30px;
        letter-spacing: 30px;
      }

      input { outline: 0 none; }
      .linkDesc {
        display: inline-flex;
        background-color: white;
        border-radius: 5px;
        font-size: x-large;
        @media screen and (max-width: 425px) {
          font-size: initial;
        }

        padding: 5px 3px;
        vertical-align: text-bottom;
        color: black;
      }
      i{
        margin: 0px 3px;
        cursor: pointer;
        color white;
        font-size: 3em;
      }
    }

    form {

      border: 1px solid #ffffff3b;
      border-radius: 5px;
      padding: 20px 10px;

      font-size: x-large;

      label::first-letter {
        text-transform: uppercase;
      }
    }

    input[type=text], textarea {
      border: none;
      border-bottom: #ffffff63 solid 1px;
      border-left: #ffffff63 solid 1px;
      background-color: #5050504f;
      outline: none;
      border-radius: 5px;
      color: white;
      padding: 5px;
      font-size: x-large;
      margin-bottom: 15px;
      width: calc(100% - 25px);
      font-family: inherit;
    }

    textarea {
      height: calc(100% - 55px);
      resize: none;
    }

    input[type=text] {
      height: 25px;
    }

    input[type=submit], input[type=button] {
      font-size: x-large;
      color: white;
      background-color: #191919;
      border-radius: 5px;
      padding: 10px 20px;
      margin-top: 30px;
      position: relative;
      left: 50%;
      transform: translateX(-50%);
      cursor: pointer;
    }

  }

`
export default style
