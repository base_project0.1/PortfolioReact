import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'

import Modal from './DefaultModal'

import 'fontAwesome'

class AddModal extends React.Component {

  constructor(props) {
    super(props)
    this.carObj = {
      veiculo: null,
      marca: null,
      ano: null,
      vendido: null,
      descricao: null,
    }
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        toggleModal={this.props.toggleModal}
      >
        <div className="title">Adicionar Veículo</div>
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title"><strong>Veículo</strong></div>
              <input
                type="text"
                placeholder="Veículo"
                onChange={e => {this.carObj.veiculo = e.target.value}}
              />
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title"><strong>Marca</strong></div>
              <input
                type="text"
                placeholder="Marca"
                onChange={e => {this.carObj.marca = e.target.value}}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title"><strong>Ano</strong></div>
              <input
                type="number"
                placeholder="Ano"
                onChange={e => {this.carObj.ano = parseInt(e.target.value)}}
              />
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title"><strong>Vendido?</strong></div>
              Não
              <input
                type="range"
                min="0"
                max="1"
                defaultValue="1"
                onChange={e => {this.carObj.vendido = e.target.value === "1" }}
              />
              Sim
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title"><strong>Descricao</strong></div>
              <textarea
                rows="4" cols="50" placeholder="Descrição"
                onChange={e => {this.carObj.descricao = e.target.value}}
              />
            </Col>
          </Row>
        </Grid>
        <hr />
        <button className="right" onClick={this.props.toggleModal}>Fechar</button>
        <button className="right" onClick={() => this.props.addCar(this.carObj)}>Adicionar</button>
      </Modal>
    )
  }
}

AddModal.propTypes = {
  show: PropTypes.bool,
  addCar: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

export default AddModal
