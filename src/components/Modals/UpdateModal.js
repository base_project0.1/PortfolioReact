import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'

import Modal from './DefaultModal'

import 'fontAwesome'

class UpdateModal extends React.Component {

  constructor(props) {
    super(props)
    this.carObj = {
      veiculo: props.car.veiculo,
      marca: props.car.marca,
      ano: props.car.ano,
      vendido: props.car.vendido,
      descricao: props.car.descricao,
    }
  }

  render() {

    const car = this.props.car

    return (
      <Modal
        show={this.props.show}
        toggleModal={this.props.toggleModal}
      >
        <div className="title">Atualizar Veículo</div>
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title">Veículo</div>
              <input
                type="text"
                placeholder={car.veiculo || "Veículo"}
                defaultValue={car.veiculo}
                onChange={e => {this.carObj.veiculo = e.target.value || car.veiculo}}
              />
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title">Marca</div>
              <input
                type="text"
                placeholder={car.marca || "Marca"}
                defaultValue={car.marca}
                onChange={e => {this.carObj.marca = e.target.value || car.marca}}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title">Ano</div>
              <input
                type="number"
                placeholder={car.ano || "Ano"}
                defaultValue={car.ano}
                onChange={e => {this.carObj.ano = parseInt(e.target.value) || car.ano}}
              />
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title">Vendido?</div>
              Não
              <input
                type="range"
                min="0"
                max="1"
                defaultValue={car.vendido ? "1" : "0"}
                onChange={e => {this.carObj.vendido = e.target.value === "1" }}
              />
              Sim
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className="sub-title">Descricao</div>
              <textarea
                rows="4" cols="50"
                placeholder={car.descricao || "Descrição"}
                defaultValue={car.descricao}
                onChange={e => {this.carObj.descricao = e.target.value || car.descricao}}
              />
            </Col>
          </Row>
        </Grid>
        <hr />
        <button className="right" onClick={this.props.toggleModal}>Fechar</button>
        <button className="right" onClick={() => this.props.updateCar(car._id, this.carObj)}>Atualizar</button>
      </Modal>
    )
  }
}

UpdateModal.propTypes = {
  show: PropTypes.bool,
  car: PropTypes.object.isRequired,
  updateCar: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

export default UpdateModal
