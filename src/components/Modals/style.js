import styled from 'styled-components'

const style = styled.div`
  position: relative;
  padding: 5px;
  font-family: Lato;

  .title {
    padding-top: 10px;
    padding-bottom: 15px;
    letter-spacing: 0em;
    font-weight: bolder;
  }

  .sub-title {
    letter-spacing: 0em;
    padding-bottom: 5px;
    padding-top: 10px;
  }

  input[type="text"], input[type="number"] {
    height: 40px;
    border: 0px solid #8c8c8c;
    border-bottom: 1px solid #8c8c8c;
  }

  input[type="range"] {
    width: 35px;
    border: 1px black solid;
  }

  textarea {
    border: 0px solid #8c8c8c;
    border-bottom: 1px solid #8c8c8c;
  }

  input[type="text"], input[type="number"], textarea {
    outline: 0;
  }

  .right {
    float: right;
  }

  button {
    min-width: 80px;
    min-height: 40px;

    background: #45535a;
    border: 1px solid #00000054;
    color: white;
    font-weight: bolder;
    text-transform: uppercase;

    padding: 5px;
    margin: 10px;

    cursor: pointer;
  }

  i {
    color: #c4ca60;
    margin-top: 30px;
  }

`
export default style
