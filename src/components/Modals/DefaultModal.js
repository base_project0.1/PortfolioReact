import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

import Modal from 'API_ComponentsBase/Modal/ReactModal'

import 'fontAwesome'

class DefaultModal extends React.Component {

  render() {

    const car = this.props.car

    return (
      <Style>
        <Modal
          isOpen={this.props.show}
          shouldCloseOnOverlayClick
          onRequestClose={this.props.toggleModal}
          style={{
            overlay: {
              backgroundColor: 'rgba(1,1,1,0.75)',
              display: 'block',
              alignItems: 'center',
              justifyContent: 'center',
              zIndex: '99',
            },
            content: {
              bottom: 'auto',
              left: '50%',
              top: '50%',
              right: 'auto',
              transform: 'translate(-50%,-50%)',
              overflow: 'visible',
            },
          }}
        >
          <Style>
            {this.props.children}
          </Style>
        </Modal>
      </Style>
    )
  }
}

DefaultModal.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.any,
  toggleModal: PropTypes.func.isRequired,
}

export default DefaultModal
