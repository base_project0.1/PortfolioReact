import styled from 'styled-components'

const StyledLoading = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    position: fixed;
    z-index: 999999;
    background-color: rgba(255, 256, 255, 1);

    background-image: radial-gradient(#e6e6e6, grey, #404040, black);

    animation: 5s ease;

    &.hide-loading {
        opacity: 0;
        z-index: -99999999;
    }
`

export default StyledLoading
