import React, { Component } from 'react'
import PropTypes from 'prop-types'

import StyledPaginationNumber from './styledPaginationNumber'
import ReactPaginate from 'react-paginate'

class index extends Component {
  render() {
    return (
      <StyledPaginationNumber>
        <ReactPaginate
          forcePage={this.props.forcePage || 0}
          pageCount={this.props.pageCount}
          pageRangeDisplayed={5}
          marginPagesDisplayed={1}
          previousLabel={<i className="fa fa-angle-up" aria-hidden="true"></i>}
          nextLabel={<i className="fa fa-angle-down" aria-hidden="true"></i>}
          breakLabel={"..."}
          breakClassName={"break-me"}
          onPageChange={(e) => { this.props.onPageChange(e.selected)}}
          containerClassName={"pagination"}
          activeClassName={"selectedPage"}
          pageClassName={"page"}
          subContainerClassName={"pages pagination"}
        />
      </StyledPaginationNumber>
    )
  }
}

index.propTypes = {
  onPageChange: PropTypes.func,
  pageCount: PropTypes.number,
  forcePage: PropTypes.number,
}

export default index
