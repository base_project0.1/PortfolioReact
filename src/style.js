import styled from 'styled-components'
import header from 'utils/header.js'
import footer from 'utils/footer.js'

const style = styled.div`

  ${'' /* position: relative; */}
  font-family: Lato;

  background-color: black;

  height: 100vh;

  .body {
    padding-top: ${header.height}px !important;
    padding-bottom: ${footer.height}px !important;
  }

  .body.disable * {
    pointer-events: none;
  }

  .body>div:not(.fixed){
    height: calc(100vh - ${header.height + footer.height}px);
    overflow-y: auto;
    overflow-x: hidden;
  }

  .body>div::-webkit-scrollbar {
    width: 10px;
  }
  
  /* Track */
  .body>div::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #ffffff8f;
    border-radius: 10px;
  }
   
  /* Handle */
  .body>div::-webkit-scrollbar-thumb {
    background: #ffffff30;
    border-radius: 10px;
  }
  
  /* Handle on hover */
  .body>div::-webkit-scrollbar-thumb:hover {
    background: #ffffff5c;
  }

  .mainContainer {
    background-color: #e2e4e1;
  }

${'' /* Genetal Styles */}
  .title {
    text-transform: uppercase;
    font-family: Montserrat;
    font-weight: bolder;
    letter-spacing: 0.5em;
  }

  .container-fluid, .row, div[class^='col-'] {
    padding: 0px;
    margin: 0px;
  }

  div {
    padding: 0px;
    margin: 0px;
    border-width: : 0px;
  }

  .captalize-txt {
    text-transform: capitalize;
  }
`
export default style
