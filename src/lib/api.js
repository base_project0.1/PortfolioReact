import Axios from 'axios'
import qs from 'qs'

const Api = {
  _axios: null,

  async init() {
    this._axios = this._getAxiosInstance()
    return true
  },

  _getAxiosInstance() {
    const params = {
      baseURL: __HOST__,
      timeout: TIMEOUT,
      headers: HEADERS
    }

    const instance = Axios.create(params)

    this.createResponseInterceptor(instance)

    return instance
  },

  getConfig() {
    return {
      headers: {
        API_TOKEN: sessionStorage.getItem("API_TOKEN"),
      },
    }
  },

  createResponseInterceptor(instance) {
    instance.interceptors.request.use((config) => {
      // Do something before request is sent
      return config
    }, (error) => {
      // Do something with request error
      return Promise.reject(error)
    })

    // Add a response interceptor
    instance.interceptors.response.use((response) => {
      // Do something with response data
      return response
    }, (error) => {
      // Do something with response error
      return Promise.reject(error);
    })
  },
  login(credentials) {
    const axios = this._getAxiosInstance()

    return axios.post('/auth/', credentials).then((response) => {

      this._axios = this._getAxiosInstance()
      return response.data
    })
  },

  logout() {
    this._axios = null
  },

  get(url, params) {
    const query = qs.stringify(params)
    return this._axios.get(`${url}?${query}`, this.getConfig())
  },

  post(url, data, config) {
    return this._axios.post(url, data, { ...this.getConfig(), ...config })
  },

  delete(url) {
    return this._axios.delete(url, this.getConfig())
  },

  put(url, data) {
    return this._axios.put(url, data, this.getConfig())
  },

  patch(url, data) {
    return this._axios.patch(url, data, this.getConfig())
  },
}
function mapStateToProps({ Authentication }) {
  return Authentication
}

export default Api
