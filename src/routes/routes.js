import config, { setPath } from './config.js'

export default {
    HomePage: config.setPath('home'),
    
    Body: config.setPath('home'),
    Projects: config.setPath('projects'),
    Curriculum: config.setPath('curriculum'),
    CV: config.setPath('cv'),
    Contact: config.setPath('contact'),
    About: config.setPath('about')
}