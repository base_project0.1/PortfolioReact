SHELL := /bin/bash # Use bash syntax

install:
	yarn install
	npm i

git-update-master:
	git fetch --all && git pull origin master

git-lab-user:
	git config credential.helper store
	git push https://gitlab.com/base_project0.1/PortfolioReact.git
	git config --global credential.helper 'cache --timeout 7200'

start:
	cp src/routes/config.dev.js src/routes/config.js
	yarn start

start-all:
	code ./
	x-www-browser http://localhost:3002/
	make start

push:
	git add . && git commit -m "${m}" && git push origin master

deploy:
	# m="" -> message
	cp src/routes/config.prod.js src/routes/config.js
	yarn build
	cp src/favicon.ico bin/favicon.ico
	cp src/.htaccess bin/.htaccess
	git add . && git commit -m "[Deploy] ${m}" && git push origin master

deploy-netlify:
	cp src/routes/config.netlify.js src/routes/config.js
	yarn build
	cp src/favicon.ico bin/favicon.ico
	# cp src/.htaccess bin/.htaccess
	cp src/_redirects bin/_redirects

